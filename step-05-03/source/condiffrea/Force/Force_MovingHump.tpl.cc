/**
 * @file Force_MovingHump.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-02-10, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <condiffrea/Force/Force_MovingHump.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace condiffrea {

template <int dim>
Force_MovingHump<dim>::
Force_MovingHump(
	const double _r0,
	const double _x0,
	const double _y0,
	std::shared_ptr< dealii::Function<dim> > epsilon,
	std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor,
	std::shared_ptr< dealii::Function<dim> > reaction
) {
	r0 = _r0;
	x0 = _x0;
	y0 = _y0;
	
	Assert(epsilon.use_count(), dealii::ExcNotInitialized());
	Assert(convection_tensor.use_count(), dealii::ExcNotInitialized());
	Assert(reaction.use_count(), dealii::ExcNotInitialized());
	
	function.epsilon = epsilon;
	function.convection_tensor = convection_tensor;
	function.reaction = reaction;
}


template <int dim>
double Force_MovingHump<dim>::value (
	const dealii::Point<dim> &p,
	const unsigned int /* component */
) const {
	Assert(dim==2, dealii::ExcNotImplemented());
	
	double val = {0};
	
	const double x = p[0];
	const double y = p[1];
	const double t = this->get_time();
	
	const auto e = function.epsilon->value(p, 0);
	const auto b = function.convection_tensor->value(p);
	const auto a = function.reaction->value(p , 0);
	
	
	const double w = r0*r0 - (x-x0)*(x-x0) - (y-y0)*(y-y0);
	const double dwdx = -2.*(x-x0);
	const double dwdy = -2.*(y-y0);
	
	const double Csin = 16.*std::sin(dealii::numbers::PI*t);
	
	const double C = 2./std::sqrt(e);
	const double Cw = C*w;
	const double atanCw = std::atan(Cw)/dealii::numbers::PI;
	
	const double u = Csin * x*(1-x)*y*(1-y) * (1./2. + atanCw);
	
	const double ux =
		Csin * (
			(1-2*x)*y*(1-y) * (1./2. + atanCw) +
			x*(1-x)*y*(1-y) * (C*dwdx/(dealii::numbers::PI*(1+Cw*Cw)))
		);
	
	const double uy =
		Csin * (
			x*(1-x)*(1-2*y) * (1./2. + atanCw) +
			x*(1-x)*y*(1-y) * (C*dwdy/(dealii::numbers::PI*(1+Cw*Cw)))
		);
	
	const double uxx =
		Csin * (
			(-2)*y*(1-y) * (1./2. + atanCw) +
			2*(1-2*x)*y*(1-y) * (C*dwdx/(dealii::numbers::PI*(1+Cw*Cw))) +
			x*(1-x)*y*(1-y) * ((
				(-2)*(1./(1+Cw*Cw) - (x-x0)/((1+Cw*Cw)*(1+Cw*Cw))*(4/e)*2*w*dwdx)
			)*C/dealii::numbers::PI)
		);
	
	const double uyy =
		Csin * (
			x*(1-x)*(-2) * (1./2. + atanCw) +
			2*x*(1-x)*(1-2*y) * (C*dwdy/(dealii::numbers::PI*(1+Cw*Cw))) +
			x*(1-x)*y*(1-y) * ((
				(-2)*(1./(1+Cw*Cw) - (y-y0)/((1+Cw*Cw)*(1+Cw*Cw))*(4/e)*2*w*dwdy)
			)*C/dealii::numbers::PI)
		);
	
	val += 16.*dealii::numbers::PI*std::cos(dealii::numbers::PI*t) *
		x*(1-x)*y*(1-y) * (1./2. + atanCw); // u_t
	
	val += -e*(uxx+uyy);      // diffusion term
	val += b[0]*ux + b[1]*uy; // convection term
	val += a * u;             // reaction term
	
	return val;
}

} // namespace

#include "Force_MovingHump.inst.in"
