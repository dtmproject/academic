/**
 * @file condiff_dG.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2017-01-31, condiff with linear reaction, varying penalty, dG, UK
 * @date 2016-08-31, condiff dG, UK
 */

/*  Copyright (C) 2012-2017 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <condiff/condiff_dG.tpl.hh>
#include <condiff/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/grid/grid_refinement.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/sparse_direct.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>


// C++ includes
#include <fstream>
#include <vector>

namespace condiff {

template<int dim>
void
condiff_dG<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
condiff_dG<dim>::
set_epsilon(std::shared_ptr< dealii::Function<dim> > epsilon) {
	function.epsilon = epsilon;
}


template<int dim>
void
condiff_dG<dim>::
set_convection(std::shared_ptr< dealii::TensorFunction<1,dim> > convection) {
	function.convection = convection;
}


template<int dim>
void
condiff_dG<dim>::
set_reaction(std::shared_ptr< dealii::Function<dim> > reaction) {
	function.reaction = reaction;
}


template<int dim>
void
condiff_dG<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
condiff_dG<dim>::
set_u_D(std::shared_ptr< dealii::Function<dim> > u_D) {
	function.u_D = u_D;
}


template<int dim>
void
condiff_dG<dim>::
set_u(std::shared_ptr< dealii::Function<dim> > u) {
	function.u = u;
}


template<int dim>
void
condiff_dG<dim>::
set_gamma(std::shared_ptr< dealii::Function<dim> > gamma) {
	function.gamma = gamma;
}


template<int dim>
void
condiff_dG<dim>::
set_data(
	const unsigned int p,
	const double S,
	const unsigned int global_refinement) {
	data.p = p;
	data.S = S;
	data.global_refinement = global_refinement;
}


template<int dim>
void
condiff_dG<dim>::
init(const unsigned int global_refinement) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(function.gamma.use_count(), dealii::ExcNotInitialized());
	
	dealii::QGauss<1> support_points(data.p+1);
// 	dealii::QGaussLobatto<1> support_points(data.p+1);
	grid->fe = std::make_shared< dealii::FE_DGQArbitraryNodes<dim> > (support_points);
	
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (data.p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
}


template<int dim>
void
condiff_dG<dim>::
reinit() {
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	
	A.reinit(*(grid->sp));
	u = std::make_shared< dealii::Vector<double> > ();
	u->reinit(grid->dof->n_dofs());
	b.reinit(grid->dof->n_dofs());
	
	f.reinit(grid->dof->n_dofs());
	u_D.reinit(grid->dof->n_dofs());
}


template<int dim>
void
condiff_dG<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	
	// Initialise the system matrix with 0.
	A = 0;
	
	{
		dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
		
		dealii::FEValues<dim> fe_values(
			*(grid->mapping),
			*(grid->fe),
			quad,
			dealii::update_values |
			dealii::update_gradients |
			dealii::update_JxW_values |
			dealii::update_quadrature_points
		);
		
		dealii::FullMatrix<double> local_A(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices(
			grid->fe->dofs_per_cell
		);
		
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		for ( ; cell != endc; ++cell) {
			fe_values.reinit(cell);
			
			local_A = 0;
			
			for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
			for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
			for (unsigned int q(0); q < quad.size(); ++q) {
				// diffusion
				local_A(i,j) +=
					fe_values.shape_grad(i,q) *
					function.epsilon->value(fe_values.quadrature_point(q)) *
					fe_values.shape_grad(j,q) *
					fe_values.JxW(q);
					
				// convection (integration by parts)
				local_A(i,j) +=
				-	fe_values.shape_grad(i,q) *
					function.convection->value(fe_values.quadrature_point(q)) *
					fe_values.shape_value(j,q) *
					fe_values.JxW(q);
				
				// linear reaction
				local_A(i,j) +=
					fe_values.shape_value(i,q) *
					function.reaction->value(fe_values.quadrature_point(q)) *
					fe_values.shape_value(j,q) *
					fe_values.JxW(q);
			}
			
			// Store the global indices into the vector local_dof_indices.
			// The cell object will give us the information.
			Assert(
				(local_dof_indices.size() == grid->fe->dofs_per_cell),
				dealii::ExcNotInitialized()
			);
			cell->get_dof_indices(local_dof_indices);
			
			// Copy the local assembly to the global matrix.
			// We use the constraint object, to set all constraints with that step.
			Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
			
			grid->constraints->distribute_local_to_global(
				local_A, local_dof_indices, A
			);
		}
	} // cell volume assemblies
	
	{
		// assemble interior penalties
		AssertThrow((dim==1), dealii::ExcNotImplemented());
		
		// to get the values and gradients on the interior and boundary nodes,
		// we use a special "quadrature rule" which has only both cell ends
		dealii::QGaussLobatto<1> face_nodes ( 2 );
		
		dealii::FEValues<dim> fe_values(
			*(grid->mapping),
			*(grid->fe),
			face_nodes,
			dealii::update_values |
			dealii::update_gradients |
			dealii::update_quadrature_points
		);
		
		dealii::FEValues<dim> fe_values_neighbor(
			*(grid->mapping),
			*(grid->fe),
			face_nodes,
			dealii::update_values |
			dealii::update_gradients |
			dealii::update_quadrature_points
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices(
			grid->fe->dofs_per_cell
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices_neighbor(
			grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ui_vi(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ue_vi(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ui_ve(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ue_ve(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		for ( ; cell != endc; ++cell) {
		for (unsigned int face_no=0; face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
		if (cell->face(face_no)->at_boundary()) {
			// boundary face
			local_ui_vi = 0;
			
			fe_values.reinit(cell);
			cell->get_dof_indices(local_dof_indices);
			
			// get normal direction
			double n = (face_no == 0) ? -1 : 1;
			
			// get penalty parameter gamma value
			const double penalty = {
				function.gamma->value(fe_values.quadrature_point(face_no))
			};
			
			for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
			for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
				local_ui_vi(i,j) +=
					penalty * fe_values.shape_value(i,face_no) *
					fe_values.shape_value(j,face_no)
				-	fe_values.shape_value(i,face_no) *
					function.epsilon->value(fe_values.quadrature_point(face_no)) *
					n * fe_values.shape_grad(j,face_no)[0]
				-	data.S * function.epsilon->value(fe_values.quadrature_point(face_no)) *
					n * fe_values.shape_grad(i,face_no)[0] *
					fe_values.shape_value(j,face_no)
				;
			}}
			
			// convection (upwind discretisation)
			// \int_F [[v]] (b \cdot n) u^{up}
			auto b = function.convection->value(fe_values.quadrature_point(face_no));
			
			if (b[0]*n >= 0.) {
				// u^{up} = u|F^+ (ui terms)
				for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
					local_ui_vi(i,j) +=
						fe_values.shape_value(i,face_no) *
						(b[0] * n) *
						fe_values.shape_value(j,face_no)
					;
				}}
			}
// 			else {
// 				// u^{up} = u|F^- (ue terms)
// 				// -> added in u_D
// 			}
			
			// copy assembly result
			grid->constraints->distribute_local_to_global(
				local_ui_vi, local_dof_indices, local_dof_indices, A
			);
		}
		else {
			// interior face
			local_ui_vi = 0;
			local_ue_vi = 0;
			local_ui_ve = 0;
			local_ue_ve = 0;
			
			// We assemble all interior face nodes only once from the
			// cell with the smaller index.
			// The assembly signs are adapted to their correct normal
			// direction: n = +1 and n_neighbor = -1
			
			auto neighbor = cell->neighbor(face_no);
			if (cell->index() < neighbor->index()) {
				// WARNING: we are on face_no == 1 on cell and
				// face_no == 0 on neighbor for dim==1.
				Assert(face_no==1, dealii::ExcInternalError());
				
				// get normal direction (only interior nodes)
				double n          = (face_no == 0) ? -1 :  1;
				double n_neighbor = (face_no == 0) ?  1 : -1;
				
				// get penalty parameter gamma value
				const double penalty = {
					function.gamma->value(fe_values.quadrature_point(face_no))
				};
				
				fe_values.reinit(cell);
				cell->get_dof_indices(local_dof_indices);
				
				fe_values_neighbor.reinit(neighbor);
				neighbor->get_dof_indices(local_dof_indices_neighbor);
				
				// diffusion
				double epsilon_value = function.epsilon->value(fe_values.quadrature_point(face_no));
				
				// assemble interior penalties
				for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
					local_ui_vi(i,j) +=
						penalty * fe_values.shape_value(i,1) *
						fe_values.shape_value(j,1)
					-	.5*fe_values.shape_value(i,1) *
						epsilon_value *
						n * fe_values.shape_grad(j,1)[0]
					-	.5*data.S *
						epsilon_value *
						n * fe_values.shape_grad(i,1)[0] *
						fe_values.shape_value(j,1)
					;
				}}
				
				for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values_neighbor.get_fe().dofs_per_cell; ++j) {
					local_ue_vi(i,j) +=
					-	penalty * fe_values.shape_value(i,1) *
						fe_values_neighbor.shape_value(j,0)
					+	.5*fe_values.shape_value(i,1) *
						epsilon_value *
						n_neighbor * fe_values_neighbor.shape_grad(j,0)[0]
					+	.5*data.S *
						epsilon_value *
						fe_values.shape_grad(i,1)[0] *
						n * fe_values_neighbor.shape_value(j,0)
					;
				}}
				
				for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
					local_ui_ve(i,j) +=
					-	penalty * fe_values_neighbor.shape_value(i,0) *
						fe_values.shape_value(j,1)
					+	.5*fe_values_neighbor.shape_value(i,0) *
						epsilon_value *
						n * fe_values.shape_grad(j,1)[0]
					+	.5*data.S *
						epsilon_value *
						n_neighbor * fe_values_neighbor.shape_grad(i,0)[0] *
						fe_values.shape_value(j,1)
					;
				}}
				
				for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values_neighbor.get_fe().dofs_per_cell; ++j) {
					local_ue_ve(i,j) +=
						penalty * fe_values_neighbor.shape_value(i,0) *
						fe_values_neighbor.shape_value(j,0)
					-	.5*fe_values_neighbor.shape_value(i,0) *
						epsilon_value *
						n_neighbor * fe_values_neighbor.shape_grad(j,0)[0]
					-	.5*data.S *
						epsilon_value *
						n_neighbor * fe_values_neighbor.shape_grad(i,0)[0] *
						fe_values_neighbor.shape_value(j,0)
					;
				}}
				
				// convection (upwind discretisation)
				// \int_F [[v]] (b \cdot n) u^{up}
				auto b = function.convection->value(fe_values.quadrature_point(face_no));
				
				if (b[0]*n >= 0.) {
					// u^{up} = u|F^+ (ui terms)
					for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
					for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
						local_ui_vi(i,j) +=
							fe_values.shape_value(i,1) *
							(b[0] * n) *
							fe_values.shape_value(j,1)
						;
					}}
					
					for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
					for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
						local_ui_ve(i,j) +=
						-	fe_values_neighbor.shape_value(i,0) *
							(b[0] * n) *
							fe_values.shape_value(j,1)
						;
					}}
				}
				else {
					// u^{up} = u|F^- (ue terms)
					for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
					for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
						local_ue_vi(i,j) +=
							fe_values.shape_value(i,1) *
							(b[0] * n) *
							fe_values_neighbor.shape_value(j,0)
						;
					}}
					
					for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
					for (unsigned int j=0; j < fe_values_neighbor.get_fe().dofs_per_cell; ++j) {
						local_ue_ve(i,j) +=
						-	fe_values_neighbor.shape_value(i,0) *
							(b[0] * n) *
							fe_values_neighbor.shape_value(j,0)
						;
					}}
				}
				
				// copy assembly results
				grid->constraints->distribute_local_to_global(
					local_ui_vi, local_dof_indices, local_dof_indices, A
				);
				
				grid->constraints->distribute_local_to_global(
					local_ue_vi, local_dof_indices, local_dof_indices_neighbor, A
				);
				
				grid->constraints->distribute_local_to_global(
					local_ui_ve, local_dof_indices_neighbor, local_dof_indices, A
				);
				
				grid->constraints->distribute_local_to_global(
					local_ue_ve, local_dof_indices_neighbor, local_dof_indices_neighbor, A
				);
			}
		}}}
	} // interior and boundary assemblies
}


template<int dim>
void
condiff_dG<dim>::
assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	f = 0;
	function.f->set_time(0.);
	
	dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
	
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->fe->dofs_per_cell
	);
	
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		fe_values.reinit(cell);
		
		local_f = 0;
		
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		cell->get_dof_indices(local_dof_indices);
		
		grid->constraints->distribute_local_to_global(
			local_f, local_dof_indices, f
		);
	}
}


template<int dim>
void
condiff_dG<dim>::
assemble_u_D() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.u_D.use_count(), dealii::ExcNotInitialized());
	// assemble weak u_D
	AssertThrow((dim==1), dealii::ExcNotImplemented());
	u_D = 0;
	function.u_D->set_time(0.);
	
	// to get the values and gradients on the interior and boundary nodes,
	// we use a special "quadrature rule" which has only both cell ends
	// x---o---o---o---o---x
	dealii::QGaussLobatto<1> face_nodes ( 2 );
	
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		face_nodes,
		dealii::update_values |
		dealii::update_gradients |
		dealii::update_quadrature_points
	);
	
	dealii::Vector<double> local_u_D(
		grid->fe->dofs_per_cell
	);
	
	dealii::Vector<double> local_projection_u_D(
		grid->fe->dofs_per_cell
	);
	
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
	if (cell->at_boundary()) {
	for (unsigned int face_no=0; face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
	if (cell->face(face_no)->at_boundary()) {
		// boundary face
		local_u_D = 0;
		
		fe_values.reinit(cell);
		cell->get_dof_indices(local_dof_indices);
		
		// get normal direction
		double n = (face_no == 0) ? -1 : 1;
		
		// get penalty parameter gamma value
		const double penalty = {
			function.gamma->value(fe_values.quadrature_point(face_no))
		};
		
		// get u_D value:
		double u_D_value = function.u_D->value(fe_values.quadrature_point(face_no));
		
		double epsilon_value = function.epsilon->value(fe_values.quadrature_point(face_no));
		
		// assemble u_D vector entries
		for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
			local_u_D(i) +=
			+	penalty * fe_values.shape_value(i,face_no) * u_D_value
			-	data.S *
				epsilon_value * n * fe_values.shape_grad(i,face_no)[0] * u_D_value
			;
		}
		
		// convection (upwind discretisation)
		// \int_F [[v]] (b \cdot n) u^{up}
		auto b = function.convection->value(fe_values.quadrature_point(face_no));
		
		if (b[0]*n < 0.) {
			// u^{up} = u|F^- (ue terms)
			// -> added in u_D
			for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
				local_u_D(i) -=
					fe_values.shape_value(i,face_no) *
					(b[0] * n) *
					u_D_value
				;
			}
		}
		
		// copy assembly result
		grid->constraints->distribute_local_to_global(
			local_u_D, local_dof_indices, u_D
		);
	}}}}
}


template<int dim>
void
condiff_dG<dim>::
solve() {
	b.equ(1., f);
	b.add(1., u_D);
	
	////////////////////////////////////////////////////////////////////////////
	// solve linear system
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(A);
	iA.vmult(*u, b);
	
	// post processing
	compute_L2_error();
}


template<int dim>
void
condiff_dG<dim>::
compute_L2_error() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	Assert(function.u.use_count(), dealii::ExcNotInitialized());
	function.u->set_time(0.);
	
	// num. exact for polynomial degree m = 2(p+1)-1 = 2p+1 (enough here, see below)
	dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
	
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
// 	double L2_error = { 0. };  // error variable
	L2_error = 0.;
	double u_h_on_xq = 0.; // variable to evaluate u_h(x_q)
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		fe_values.reinit(cell);
		
		cell->get_dof_indices(local_dof_indices);
		
		for (unsigned int q(0); q < quad.size(); ++q) {
			u_h_on_xq = 0.;
			for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j) {
				u_h_on_xq += (*u)[local_dof_indices[j]] * fe_values.shape_value(j,q);
			}
			
			// polynomial degree to be integrated: 2*p
			L2_error +=
				(function.u->value(fe_values.quadrature_point(q), 0) - u_h_on_xq) *
				(function.u->value(fe_values.quadrature_point(q), 0) - u_h_on_xq) *
				fe_values.JxW(q);
		}
	}
	
	L2_error = std::sqrt(L2_error);
	std::cout << "|| u - u_h ||_L^2(Omega) = " << L2_error << std::endl;
	DTM::pout << "|| u - u_h ||_L^2(Omega) = " << L2_error << std::endl;
}


template<int dim>
void
condiff_dG<dim>::
do_data_output() {
	{
		////////////////////////////////////////////////////////////////////////////
		// write solution nodal values to file
		
		dealii::QGaussLobatto<dim> quad ( 2+grid->fe->tensor_degree()+1 );
// 		dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
		
		dealii::FEValues<dim> fe_values(
			*(grid->mapping),
			*(grid->fe),
			quad,
			dealii::update_values |
			dealii::update_quadrature_points
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices(
			grid->fe->dofs_per_cell
		);
		
		std::map<unsigned int, std::pair< dealii::Point<dim>, double > > nodal_values;
		
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		unsigned int point_no = {1};
		for ( ; cell != endc; ++cell) {
			cell->get_dof_indices(local_dof_indices);
			fe_values.reinit(cell);
			
			for (unsigned int q=0; q < fe_values.get_quadrature().size(); ++q) {
				double value = {0};
				
				for (unsigned int j={0}; j < fe_values.get_fe().dofs_per_cell; ++j) {
					value += (*u)[local_dof_indices[j]] * fe_values.shape_value(j,q);
				}
				
				nodal_values.insert(
					std::make_pair(
						point_no++,
						std::make_pair(
							fe_values.quadrature_point(q),
							value
						)
					)
				);
			}
		}
		
		std::ofstream output_file(
			static_cast< std::string >("u.log").c_str()
		);
		
		for (unsigned int d=0; d < dim; ++d) {
			output_file << "x" << (d+1) << " ";
		}
		
		output_file << "u" << std::endl;
		for (const auto &nodal_value : nodal_values) {
			for (unsigned int d=0; d < dim; ++d) {
				output_file << nodal_value.second.first[d] << " ";
			}
			
			output_file << nodal_value.second.second << std::endl;
		}
		
		output_file.close();
	}
}


template<int dim>
void
condiff_dG<dim>::
run() {
	init(data.global_refinement);
	reinit();
	
	assemble_system();
	assemble_f();
	assemble_u_D();
	solve();
	
	// write out the solution
	do_data_output();
	
}

} // namespace

#include "condiff_dG.inst.in"
