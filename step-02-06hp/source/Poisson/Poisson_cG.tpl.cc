/**
 * @file Poisson_cG.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-22, modifications for Dirichlet, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief Poisson Problem
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Poisson/Poisson_cG.tpl.hh>
#include <Poisson/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/numerics/data_out.h>

// new: (interpolate boundary values, apply boundary values and direct solver)
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/lac/sparse_direct.h>

// new: (grid refinement, error estimators)
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/numerics/error_estimator.h>

// C++ includes
#include <fstream>
#include <string>
#include <vector>

namespace Poisson {

template<int dim>
Poisson_cG<dim>::
Poisson_cG(
	const unsigned int p_min,
	const unsigned int p_max) : p_min(p_min), p_max(p_max) {
}


template<int dim>
void
Poisson_cG<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
Poisson_cG<dim>::
set_c(std::shared_ptr< dealii::Function<dim> > c) {
	function.c = c;
}


template<int dim>
void
Poisson_cG<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
Poisson_cG<dim>::
set_g(std::shared_ptr< dealii::Function<dim> > g) {
	function.g = g;
}


template<int dim>
void
Poisson_cG<dim>::
set_h(std::shared_ptr< dealii::Function<dim> > h) {
	function.h = h;
}


template<int dim>
void
Poisson_cG<dim>::
run(const unsigned int global_refinement) {
	init(global_refinement);
	
	for (unsigned refinement_cycle=0; refinement_cycle < 1; ++refinement_cycle) {
		DTM::pout << "hp cycle = " << refinement_cycle << std::endl;
		std::cout << "hp cycle = " << refinement_cycle << std::endl;
		
		if (refinement_cycle) {
			estimate_error_and_mark();
			reinit();
		}
		
		assemble_system();
		assemble_f();
		
		solve(refinement_cycle);
	}
}


template<int dim>
void
Poisson_cG<dim>::
init(const unsigned int global_refinement) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	
	// generate hp FE_Q-GLL finite elements and corresponding mappings
	grid->fe_collection = std::make_shared< dealii::hp::FECollection<dim> > ();
	grid->mapping_collection = std::make_shared< dealii::hp::MappingCollection<dim> > ();
	
	for (unsigned int degree = p_min; degree <= p_max; ++degree) {
		Assert(grid->fe_collection.use_count(), dealii::ExcNotInitialized());
		Assert(grid->mapping_collection.use_count(), dealii::ExcNotInitialized());
		
		dealii::QGaussLobatto<1> support_points(degree+1);
		grid->fe_collection->push_back(
			dealii::FE_Q<dim> (support_points)
		);
		
		grid->mapping_collection->push_back(
			dealii::MappingQ<dim> (degree)
		);
	}
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u.reinit(grid->dof->n_dofs());
	b.reinit(grid->dof->n_dofs());
	f.reinit(grid->dof->n_dofs());
	h.reinit(grid->dof->n_dofs());
	
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	A.reinit(*(grid->sp));
}

template<int dim>
void
Poisson_cG<dim>::
estimate_error_and_mark() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->tria.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe_collection.use_count(), dealii::ExcNotInitialized());
	Assert(u.size(), dealii::ExcNotInitialized());
	
	dealii::Vector<float> estimated_error_per_cell;
	estimated_error_per_cell.reinit(grid->tria->n_active_cells());
	
	// Setup hp-quadrature face-formula
	dealii::hp::QCollection<dim-1> quad_face_collection;
	{
		for (unsigned int fe_no = 0; fe_no < grid->fe_collection->size(); ++fe_no) {
			dealii::QGauss<dim-1> quad_face (
				grid->fe_collection->operator[](fe_no).tensor_degree()+1
			);
			
			quad_face_collection.push_back(quad_face);
		}
	}
	
	// estimate errors
	dealii::KellyErrorEstimator<dim>::estimate(
		*grid->dof,
		quad_face_collection,
		{}, // TODO: map for Neumann bnd
		u, // solution vector
		estimated_error_per_cell
		// TODO other inputs
	);
	
	// TODO: estimate smoothness
	
	// mark cells for refinement
	dealii::GridRefinement::refine_and_coarsen_fixed_fraction(
		*grid->tria,
		estimated_error_per_cell,
		0.8, // top fraction for refinement
		0.0 // bottom fraction for coarsening
		// TODO: max. no. of cells after refinement
	);
}

template<int dim>
void
Poisson_cG<dim>::
reinit() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	
	// reinit tria of grid and distribute dofs
	grid->tria->execute_coarsening_and_refinement();
	grid->set_boundary_indicators();
	
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u.reinit(grid->dof->n_dofs());
	b.reinit(grid->dof->n_dofs());
	f.reinit(grid->dof->n_dofs());
	h.reinit(grid->dof->n_dofs());
	
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	A.reinit(*(grid->sp));
}

template<int dim>
void
Poisson_cG<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe_collection.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping_collection.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.c.use_count(), dealii::ExcNotInitialized());
	
	A = 0;
	
	// Setup hp-quadrature formula
	dealii::hp::QCollection<dim> quad_collection;
	{
		for (unsigned int fe_no = 0; fe_no < grid->fe_collection->size(); ++fe_no) {
			dealii::QGaussLobatto<dim> quad (
				grid->fe_collection->operator[](fe_no).tensor_degree()+1
			);
			
			quad_collection.push_back(quad);
		}
	}
	
	// Setup hp::FEValues object
	dealii::hp::FEValues<dim> hp_fe_values(
		*(grid->mapping_collection),
		*(grid->fe_collection),
		quad_collection,
		dealii::update_gradients |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::FullMatrix<double> local_A;
	std::vector< dealii::types::global_dof_index > local_dof_indices;
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		hp_fe_values.reinit(cell);
		
		auto &local_fe = cell->get_fe();
		auto &local_fe_values = hp_fe_values.get_present_fe_values();
		auto &local_quad = local_fe_values.get_quadrature();
		
		// Initialise the full matrix for the cell assembly with 0.
		local_A.reinit(local_fe.dofs_per_cell, local_fe.dofs_per_cell);
		local_A = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < local_fe.dofs_per_cell; ++i)
		for (unsigned int j(0); j < local_fe.dofs_per_cell; ++j)
		for (unsigned int q(0); q < local_quad.size(); ++q) {
			local_A(i,j) +=
				local_fe_values.shape_grad(i,q) *
				function.c->value(local_fe_values.quadrature_point(q), 0) *
				local_fe_values.shape_grad(j,q) *
				local_fe_values.JxW(q);
		}
		
		// Resize and store the local-to-global indices object
		local_dof_indices.resize(local_fe.dofs_per_cell);
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix
		grid->constraints->distribute_local_to_global(
			local_A,
			local_dof_indices,
			A
		);
	}
}


template<int dim>
void
Poisson_cG<dim>::
assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe_collection.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping_collection.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	
	f = 0;
	
	// Setup hp-quadrature formula
	dealii::hp::QCollection<dim> quad_collection;
	{
		for (unsigned int fe_no = 0; fe_no < grid->fe_collection->size(); ++fe_no) {
			dealii::QGaussLobatto<dim> quad (
				grid->fe_collection->operator[](fe_no).tensor_degree()+1
			);
			
			quad_collection.push_back(quad);
		}
	}
	
	// Setup hp::FEValues object
	dealii::hp::FEValues<dim> hp_fe_values(
		*(grid->mapping_collection),
		*(grid->fe_collection),
		quad_collection,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f;
	std::vector< dealii::types::global_dof_index > local_dof_indices;
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		hp_fe_values.reinit(cell);
		
		auto &local_fe = cell->get_fe();
		auto &local_fe_values = hp_fe_values.get_present_fe_values();
		auto &local_quad = local_fe_values.get_quadrature();
		
		// Initialise the full matrix for the cell assembly with 0.
		local_f.reinit(local_fe.dofs_per_cell);
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < local_fe.dofs_per_cell; ++i)
		for (unsigned int q(0); q < local_quad.size(); ++q) {
			local_f(i) +=
				local_fe_values.shape_value(i,q) *
				function.f->value(local_fe_values.quadrature_point(q), 0) *
				local_fe_values.JxW(q);
		}
		
		// Resize and store the local-to-global indices object
		local_dof_indices.resize(local_fe.dofs_per_cell);
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix
		grid->constraints->distribute_local_to_global(
			local_f,
			local_dof_indices,
			f
		);
	}
}


template<int dim>
void
Poisson_cG<dim>::
solve(const unsigned int refinement_cycle) {
	b = 0;
	b.add(1., f);
	
	// fetch Dirichlet boundary values for the Dirichlet-marked dofs by interpolation:
	std::map<dealii::types::global_dof_index, double> boundary_values;
	
	Assert(function.g.use_count(), dealii::ExcNotInitialized());
	function.g->set_time(0.);
	
	dealii::VectorTools::interpolate_boundary_values(
		*grid->dof,
		static_cast< dealii::types::boundary_id > (
			Poisson::types::boundary_id::Dirichlet
		),
		*function.g,
		boundary_values // final object
	);
	
	// apply Dirichlet boundary values to the system:
	dealii::MatrixTools::apply_boundary_values(
		boundary_values,
		A, u, b
	);
	
	// solve the linear system Au = b
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(A);
	iA.vmult(u, b); // solve linear system
	
	grid->constraints->distribute(u);
	
	write_vtk("u", u, refinement_cycle);
}


template<int dim>
void
Poisson_cG<dim>::
write_vtk(
	std::string name,
	dealii::Vector<double> &solution_vector,
	const unsigned int refinement_cycle) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	
	std::ofstream output_file(
		(name+"-"+std::to_string(refinement_cycle)+".vtk").c_str()
	);
	
	dealii::DataOut<dim, dealii::hp::DoFHandler<dim>> data_out;
	data_out.attach_dof_handler(*(grid->dof));
	data_out.add_data_vector(solution_vector, name);
	
	dealii::Vector<float> fe_degrees(grid->tria->n_active_cells());
	for (const auto &cell : grid->dof->active_cell_iterators()) {
		fe_degrees(cell->active_cell_index()) =
			grid->fe_collection->operator[](cell->active_fe_index()).degree;
	}
	data_out.add_data_vector(fe_degrees, "fe_degree");
	
	data_out.build_patches(); // TODO
// 	data_out.build_patches(
// 		std::max(
// 			static_cast<unsigned int> (1),
// 			grid->fe_collection->max_degree()
// 		)
// 	);
	
	data_out.write_vtk(output_file);
}

} // namespace

#include "Poisson_cG.inst.in"
