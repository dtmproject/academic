/**
 * @file Grids.tpl.cc
 * @author Uwe Koecher
 * @date 2015-11-11, UK
 * @date 2015-05-15, UK
 * @date 2013-08-15, UK
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// Project includes
#include <Poisson/Grid/Grids.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/fe/mapping_q.h>

// C++ includes
#include <vector>

// class declaration
namespace Poisson {

////////////////////////////////////////////////////////////////////////////////
// Grid4

template<int dim, int spacedim>
void
Grid4<dim,spacedim>::
generate() {
	const double a(-1.);
	const double b( 1.);
	
	dealii::GridGenerator::hyper_cube(
		*Grid<dim,spacedim>::tria,
		a,b,false
	);
}


template<int dim, int spacedim>
void
Grid4<dim,spacedim>::
set_boundary_indicators() {
	Assert((Grid<dim,spacedim>::tria.use_count()), dealii::ExcNotInitialized());
	
	// set boundary indicators
	auto cell(Grid<dim,spacedim>::tria->begin_active());
	auto endc(Grid<dim,spacedim>::tria->end());
	
	for (; cell != endc; ++cell) {
	if (cell->at_boundary()) {
	for (unsigned int face(0); face < dealii::GeometryInfo<dim>::faces_per_cell; ++face) {
		if (cell->face(face)->at_boundary()) {
			cell->face(face)->set_boundary_id(
				static_cast<dealii::types::boundary_id> (
					Poisson::types::boundary_id::Dirichlet)
			);
		}
	}}}
}


////////////////////////////////////////////////////////////////////////////////
// Grid5

template<int dim, int spacedim>
void
Grid5<dim,spacedim>::
generate() {
	const double a(-1.);
	const double b( 1.);
	
	dealii::GridGenerator::hyper_cube(
		*Grid<dim,spacedim>::tria,
		a,b,false
	);
}


template<int dim, int spacedim>
void
Grid5<dim,spacedim>::
set_boundary_indicators() {
	Assert((Grid<dim,spacedim>::tria.use_count()), dealii::ExcNotInitialized());
	
	// set boundary indicators
	auto cell(Grid<dim,spacedim>::tria->begin_active());
	auto endc(Grid<dim,spacedim>::tria->end());
	
	for (; cell != endc; ++cell) {
	if (cell->at_boundary()) {
	for (unsigned int face(0); face < dealii::GeometryInfo<dim>::faces_per_cell; ++face) {
		if (cell->face(face)->at_boundary()) {
			auto center = cell->face(face)->center();
			
			if (center[0] == -1) {
				cell->face(face)->set_boundary_id(
					static_cast<dealii::types::boundary_id> (
						Poisson::types::boundary_id::Neumann)
				);
			}
			else {
				cell->face(face)->set_boundary_id(
					static_cast<dealii::types::boundary_id> (
						Poisson::types::boundary_id::Dirichlet)
				);
			}
		}
	}}}
}

//
////////////////////////////////////////////////////////////////////////////////

} // namespaces

#include "Grids.inst.in"
