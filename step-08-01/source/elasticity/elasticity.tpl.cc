/**
 * @file elasticity.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-13, elasticity, UK
 * @date 2016-08-11, Poisson/DWR, UK
 * @date 2016-02-10, condiffrea/SUPG, UK
 * @date 2016-01-15, condiff/SUPG, UK
 * @date 2016-01-14, condiff, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <elasticity/elasticity.tpl.hh>
#include <elasticity/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/grid/grid_refinement.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/sparse_direct.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/matrix_tools.h>

// C++ includes
#include <fstream>
#include <vector>

namespace vectorvalued {

template<int dim>
void
elasticity<dim>::
set_grid(std::shared_ptr< Grid<dim> > _grid) {
	grid = _grid;
}


template<int dim>
void
elasticity<dim>::
set_f(
	std::shared_ptr< dealii::Function<dim> > f,
	std::shared_ptr< dealii::Function<dim> > lambda,
	std::shared_ptr< dealii::Function<dim> > mu) {
	function.f = f;
	function.lambda = lambda;
	function.mu = mu;
}


template<int dim>
void
elasticity<dim>::
set_data(
	const unsigned int p_primal,
	const unsigned int global_refinement) {
	data.p_primal = p_primal;
	data.global_refinement = global_refinement;
}


template<int dim>
void
elasticity<dim>::
set_data_output_patches(
	unsigned int _data_output_patches_primal) {
	primal.data_output_patches = _data_output_patches_primal;
}


template<int dim>
void
elasticity<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	
	grid->primal.fe = std::make_shared< dealii::FESystem<dim> > (
		dealii::FE_Q<dim> (data.p_primal), dim
	);
	
	grid->primal.mapping = std::make_shared< dealii::MappingQ<dim> > (data.p_primal);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	
	////////////////////////////////////////////////////////////////////////////
	// INIT DATA OUTPUT
	//
	Assert(grid->primal.dof.use_count(), dealii::ExcNotInitialized());
	
	DTM::pout << "primal solution data output: patches = " << primal.data_output_patches << std::endl;
	
	{
		std::vector<std::string> data_field_names;
		std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci_field;
		
		for (unsigned int d{0}; d < dim; ++d) {
			data_field_names.push_back("displacement");
			dci_field.push_back(dealii::DataComponentInterpretation::component_is_part_of_vector);
		}
		
		primal.data_output.set_DoF_data(
			grid->primal.dof
		);
		
		primal.data_output.set_data_field_names(data_field_names);
		primal.data_output.set_data_component_interpretation_field(dci_field);
		primal.data_output.set_data_output_patches(primal.data_output_patches);
	}
}


template<int dim>
void
elasticity<dim>::
reinit() {
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->primal.dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.sp.use_count(), dealii::ExcNotInitialized());
	
	primal.A.reinit(*(grid->primal.sp));
	primal.u = std::make_shared< dealii::Vector<double> > ();
	primal.u->reinit(grid->primal.dof->n_dofs());
	primal.f.reinit(grid->primal.dof->n_dofs());
}


template<int dim>
void
elasticity<dim>::
primal_assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.lambda.use_count(), dealii::ExcNotInitialized());
	Assert(function.mu.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	
	// Initialise the system matrix with 0.
	primal.A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points
	dealii::QGauss<dim> quad ( grid->primal.fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->primal.mapping),
		*(grid->primal.fe),
		quad,
		dealii::update_gradients | // update shape function gradient values
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_A(
		grid->primal.fe->dofs_per_cell, grid->primal.fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->primal.fe->dofs_per_cell
	);
	
	function.lambda->set_time(0.);
	std::vector< double > lambda_values(quad.size());
	
	function.mu->set_time(0.);
	std::vector< double > mu_values(quad.size());
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	auto cell = grid->primal.dof->begin_active();
	auto endc = grid->primal.dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix for the cell assembly with 0.
		local_A = 0;
		
		function.lambda->value_list(
			fe_values.get_quadrature_points(),
			lambda_values
		);
		
		function.mu->value_list(
			fe_values.get_quadrature_points(),
			mu_values
		);
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->primal.fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->primal.fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			// weak linear elasticity operator
			local_A(i,j) +=
				(
					fe_values.shape_grad(i,q)[grid->primal.fe->system_to_component_index(i).first] *
					lambda_values[q] * 
					fe_values.shape_grad(j,q)[grid->primal.fe->system_to_component_index(j).first]
				
				+ 
					fe_values.shape_grad(i,q)[grid->primal.fe->system_to_component_index(j).first] *
					mu_values[q] * 
					fe_values.shape_grad(j,q)[grid->primal.fe->system_to_component_index(i).first]
				
				+	((grid->primal.fe->system_to_component_index(i).first ==
					grid->primal.fe->system_to_component_index(j).first) ? 1. : 0. ) *
					fe_values.shape_grad(i,q) *
					mu_values[q] *
					fe_values.shape_grad(j,q)
				) * fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		Assert(
			(local_dof_indices.size() == grid->primal.fe->dofs_per_cell),
			dealii::ExcNotInitialized()
		);
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		Assert(grid->primal.constraints.use_count(), dealii::ExcNotInitialized());
		
		grid->primal.constraints->distribute_local_to_global(
			local_A, local_dof_indices, primal.A
		);
	}
}


template<int dim>
void
elasticity<dim>::
primal_assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	
	primal.f = 0;
	function.f->set_time(0.);
	
	dealii::QGauss<dim> quad ( grid->primal.fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->primal.mapping),
		*(grid->primal.fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->primal.fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standard template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->primal.fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->primal.dof->begin_active();
	auto endc = grid->primal.dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->primal.fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(
					fe_values.quadrature_point(q),
					grid->primal.fe->system_to_component_index(i).first
				) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->primal.constraints->distribute_local_to_global(
			local_f, local_dof_indices, primal.f
		);
	}
}


template<int dim>
void
elasticity<dim>::
primal_solve() {
	////////////////////////////////////////////////////////////////////////////
	// apply Dirichlet boundary values
	std::map<dealii::types::global_dof_index, double> boundary_values;
	dealii::VectorTools::interpolate_boundary_values(
		*grid->primal.dof,
		static_cast< dealii::types::boundary_id > (
			vectorvalued::types::elasticity::boundary_id::Dirichlet
		),
		dealii::Functions::ZeroFunction<dim> (dim),
		boundary_values
	);
	
	dealii::MatrixTools::apply_boundary_values(
		boundary_values,
		primal.A,
		*primal.u,
		primal.f
	);
	
	////////////////////////////////////////////////////////////////////////////
	// solve linear system
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(primal.A);
	iA.vmult(*primal.u, primal.f);
	
	////////////////////////////////////////////////////////////////////////////
	// distribute hanging node constraints on solution
	grid->primal.constraints->distribute(*primal.u);
}


template<int dim>
void
elasticity<dim>::
do_data_output(const double cycle) {
	const double solution_time = cycle;
	
	primal.data_output.write_data("solution", primal.u, solution_time);
}


template<int dim>
void
elasticity<dim>::
refine_grid() {
	auto error_indicators = std::make_shared< dealii::Vector<float> > ();
	error_indicators->reinit(grid->tria->n_active_cells());
	
	dealii::KellyErrorEstimator<dim>::estimate(
		*grid->primal.dof,
		dealii::QGauss<dim-1>(data.p_primal+1),
		{},
		*primal.u,
		*error_indicators
	);
	
	dealii::GridRefinement::refine_and_coarsen_fixed_number(
		*grid->tria,
		*error_indicators,
		0.3,
		0.03
	);
	
	grid->tria->execute_coarsening_and_refinement();
}


template<int dim>
void
elasticity<dim>::
run(const unsigned int refinement_cycles) {
	init(data.global_refinement);
	
	for (unsigned int cycle{0}; cycle < refinement_cycles; ++cycle) {
		if (cycle)
			refine_grid();
		
		reinit();
		
		primal_assemble_system();
		primal_assemble_f();
		primal_solve();
		
		// write out the solution
		do_data_output(cycle);
	}
}

} // namespace

#include "elasticity.inst.in"
