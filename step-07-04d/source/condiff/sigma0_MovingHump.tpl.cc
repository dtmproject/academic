/**
 * @file sigma0_MovingHump.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2017-02-01, UK
 */

/*  Copyright (C) 2012-2017 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <condiff/sigma0_MovingHump.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace condiff {

template <int dim>
sigma0_MovingHump<dim>::
sigma0_MovingHump(
	const double _epsilon_high,
	const double _epsilon_low
) {
	epsilon_high = _epsilon_high;
	epsilon_low = _epsilon_low;
}


template <int dim>
double sigma0_MovingHump<dim>::value (
	const dealii::Point<dim> &p,
	const unsigned int /* component */
) const {
	Assert(dim==1, dealii::ExcNotImplemented());
	
	double val = epsilon_high;
	
	const double x = p[0];
// 	const double t = this->get_time();
// 	const double t = {1./2.}; // sin(pi t) = 1 (stationary case)
	
// 	if ((x > 0.2) && (x < 0.3)) {
// 		val = epsilon_low;
// 	}
// 	
// 	if ((x > 0.7) && (x < 0.8)) {
// 		val = epsilon_low;
// 	}
	
	if ((x > 0.05) && (x < .95)) {
		val = epsilon_low;
	}
	
	return val;
}

} // namespace

#include "sigma0_MovingHump.inst.in"
