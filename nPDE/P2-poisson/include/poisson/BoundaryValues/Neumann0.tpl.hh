/**
 * @file Neumann0.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-10-12, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Neumann0_tpl_hh
#define __Neumann0_tpl_hh

// PROJECT includes
#include <DTM++/base/Function.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>

// C++ includes

namespace poisson {

template<int dim>
class Neumann0 : public DTM::Function<dim,1> {
public:
	Neumann0(std::shared_ptr< dealii::Function<dim> > c) {
		function.c = c;
	}
	
	virtual ~Neumann0() = default;
	
	virtual double value (
		const dealii::Point<dim> &x,
		const unsigned int component = 0
	) const;
	
protected:
	struct {
		std::shared_ptr< dealii::Function<dim> > c;
	} function;
};

} // namespace

#endif
