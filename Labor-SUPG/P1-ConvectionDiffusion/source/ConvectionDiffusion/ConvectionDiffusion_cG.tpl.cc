/**
 * @file ConvectionDiffusion_cG.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2021-10-20, UK
 * @date 2016-08-15, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief ConvectionDiffusion Problem
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <ConvectionDiffusion/ConvectionDiffusion_cG.tpl.hh>
#include <ConvectionDiffusion/Grid/boundary_id.hh>

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_bicgstab.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/numerics/data_out.h>

#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/vector_tools.h>

// C++ includes
#include <fstream>
#include <vector>

namespace ConvectionDiffusion {

template<int dim>
ConvectionDiffusion_cG<dim>::
ConvectionDiffusion_cG(const unsigned int p) : p(p) {
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
set_g(std::shared_ptr< dealii::Function<dim> > g) {
	function.g = g;
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
set_epsilon(std::shared_ptr< dealii::Function<dim> > epsilon) {
	function.epsilon = epsilon;
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
set_convection_tensor(
	std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor) {
	function.convection_tensor = convection_tensor;
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
run(const unsigned int global_refinement) {
	init(global_refinement);
	assemble_system();
	assemble_f();
	solve();
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q-GLL finite element and boundary mapping
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	dealii::QGaussLobatto<1> support_points(p+1);
	grid->fe = std::make_shared< dealii::FE_Q<dim> > (support_points);
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	u.reinit(grid->dof->n_dofs());
	b.reinit(grid->dof->n_dofs());
	f.reinit(grid->dof->n_dofs());
	
	A.reinit(*(grid->sp)); // initialise sparsity pattern of matrix A
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	Assert(function.epsilon.use_count(), dealii::ExcNotInitialized());
	Assert(function.convection_tensor.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	// NOTE: see deal.II step-3 tutorial for details
	// Here, we want to assemble the system matrix of the weak Laplacian operator.
	// Due to FEM, this can be done by summing up local contributions over
	// all cells and trial/test function combinations.
	// The inner most integral will be solved through a numical approximation
	// by a Gaussian quadrature rule.
	
	// Initialise the system matrix entries with 0.
	A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_gradients |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_A(
		grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix for the cell assembly with 0.
		local_A = 0;
		
		// diffusion
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_A(i,j) +=
				fe_values.shape_grad(i,q) *
				function.epsilon->value(fe_values.quadrature_point(q), 0) *
				fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
		}
		
		// convection
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_A(i,j) +=
				fe_values.shape_value(i,q) *
				function.convection_tensor->value(fe_values.quadrature_point(q)) *
				fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_A, local_dof_indices, A
		);
	}
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	f = 0;
	
	dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_f, local_dof_indices, f
		);
	}
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
solve() {
	// prepare rhs
	b = f;
	
	// apply Dirichlet boundary values
	std::map< dealii::types::boundary_id, const dealii::Function<dim>* > boundary_function_map;
	boundary_function_map[static_cast<dealii::types::boundary_id> (
		ConvectionDiffusion::types::boundary_id::Dirichlet)] = function.g.get();
	
	std::map< dealii::types::global_dof_index, double > boundary_values_map;
	
	// Now we interpolate the Dirichlet boundary functions onto the FE space.
	dealii::VectorTools::interpolate_boundary_values(
		*grid->mapping,
		*grid->dof,
		boundary_function_map, boundary_values_map
	);
		
	// Here we apply the Dirichlet boundary to the linear system.
	dealii::MatrixTools::apply_boundary_values(boundary_values_map, A, u, b, false);
	
// 	std::cout << "A = " << std::endl;
// 	A.print_formatted(std::cout,1,false);
// 	std::cout << std::endl;
// 	
// 	std::cout << "b = " << std::endl;
// 	b.print(std::cout);
// 	std::cout << std::endl;
	
	// solve the linear system Au = b.
	dealii::SolverControl sc(
		std::max(1000, static_cast<int>(A.m())),
		1.0e-10, false, true
	);

	dealii::SolverBicgstab< > lss(sc);
	
	lss.solve(
		A,u,b,
		dealii::PreconditionIdentity()
	);
	
	////////////////////////////////////////////////////////////////////////////
	// distribute hanging node constraints on solution
	grid->constraints->distribute(u);
	
// 	std::cout << "u = " << std::endl;
// 	u.print(std::cout);
// 	std::cout << std::endl;
	
	write_vtk("u", u);
	
	////////////////////////////////////////////////////////////////////////////
	// write solution nodal values to file
	
// 	dealii::QGaussLobatto<dim> quad ( 2 );
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points
	);
	
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	std::map<unsigned int, std::pair< dealii::Point<dim>, double > > nodal_values;
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	unsigned int point_no = {1};
	for ( ; cell != endc; ++cell) {
		cell->get_dof_indices(local_dof_indices);
		fe_values.reinit(cell);
		
		for (unsigned int q=0; q < fe_values.get_quadrature().size(); ++q) {
			double value = {0};
			
			for (unsigned int j={0}; j < fe_values.get_fe().dofs_per_cell; ++j) {
				value += u[local_dof_indices[j]] * fe_values.shape_value(j,q);
			}
			
			nodal_values.insert(
				std::make_pair(
					point_no++,
					std::make_pair(
						fe_values.quadrature_point(q),
						value
					)
				)
			);
		}
	}
	
	std::ofstream output_file(
		static_cast< std::string >("u.log").c_str()
	);
	
	for (unsigned int d=0; d < dim; ++d) {
		output_file << "x" << (d+1) << " ";
	}
	
	output_file << "u" << std::endl;
	for (const auto &nodal_value : nodal_values) {
		for (unsigned int d=0; d < dim; ++d) {
			output_file << nodal_value.second.first[d] << " ";
		}
		
		output_file << nodal_value.second.second << std::endl;
	}
	
	output_file.close();
}


template<int dim>
void
ConvectionDiffusion_cG<dim>::
write_vtk(std::string name, dealii::Vector<double> &solution_vector) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	
	std::ofstream output_file(
		(name+".vtk").c_str()
	);
	
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*(grid->dof));
	data_out.add_data_vector(solution_vector, name);
	data_out.build_patches(grid->fe->tensor_degree());
	
	data_out.write_vtk(output_file);
}

} // namespace

#include "ConvectionDiffusion_cG.inst.in"
