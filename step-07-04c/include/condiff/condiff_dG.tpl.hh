/**
 * @file condiff_dG.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2017-01-31, condiff with linear reaction, varying penalty, dG, UK
 * @date 2016-08-31, condiff dG, UK
 */

/*  Copyright (C) 2012-2017 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __condiff_dG_tpl_hh
#define __condiff_dG_tpl_hh

// PROJECT includes
#include <condiff/Grid/Grid.tpl.hh>

// DTM++ includes

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/fe/fe.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace condiff {

template<int dim>
class condiff_dG {
public:
	condiff_dG() = default;
	virtual ~condiff_dG() = default;
	
	virtual void set_grid(std::shared_ptr< condiff::Grid<dim,1> > grid);
	
	virtual void set_epsilon(std::shared_ptr< dealii::Function<dim> > epsilon);
	virtual void set_convection(std::shared_ptr< dealii::TensorFunction<1,dim> > convection);
	virtual void set_reaction(std::shared_ptr< dealii::Function<dim> > reaction);
	
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	
	virtual void set_u_D(std::shared_ptr< dealii::Function<dim> > u_D);
	
	virtual void set_u(std::shared_ptr< dealii::Function<dim> > u); ///< exact solution (if any)
	
	// penalty
	virtual void set_gamma(std::shared_ptr< dealii::Function<dim> > gamma);
	
	virtual void set_data(
		const unsigned int p,
		const double S,
		const unsigned int global_refinement
	);
	
	virtual void run();
	double L2_error;

protected:
	virtual void init(const unsigned int global_refinement);
	virtual void reinit();
	
	// primal problem
	virtual void assemble_system();
	virtual void assemble_f();
	virtual void assemble_u_D();
	
	virtual void solve();
	
	virtual void compute_L2_error();
	
	virtual void do_data_output();
	
	dealii::SparseMatrix<double> A; ///< primal problem system matrix
	std::shared_ptr< dealii::Vector<double> > u; ///< primal problem solution
	dealii::Vector<double> b;       ///< primal problem rhs
	
	dealii::Vector<double> f;
	dealii::Vector<double> u_D;
	
	std::shared_ptr< condiff::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > epsilon;
		std::shared_ptr< dealii::TensorFunction<1,dim> > convection;
		std::shared_ptr< dealii::Function<dim> > reaction;
		
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > u_D; ///< Dirichlet boundary function.
		
		std::shared_ptr< dealii::Function<dim> > u; ///< exact solution u function (if any).
		
		std::shared_ptr< dealii::Function<dim> > gamma; ///< penalty function
	} function;
	
	struct {
		unsigned int p;
		double S;
		unsigned int global_refinement;
	} data;
};

} // namespace

#endif
