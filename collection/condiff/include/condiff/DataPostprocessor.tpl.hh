/**
 * @file   DataPostprocessor.tpl.hh
 * @author Uwe Koecher (UK)
 * @date   2016-04-06, academic/heat, UK
 * @date   2015-02-24, CCFD, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __DataPostprocessor_tpl_hh
#define __DataPostprocessor_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/base/point.h>
#include <deal.II/base/tensor.h>
#include <deal.II/base/function.h>

#include <deal.II/lac/vector.h>

#include <deal.II/numerics/data_postprocessor.h>


// C++ includes
#include <string>
#include <vector>
#include <memory>


namespace condiff {

enum OutputQuantities : unsigned int {
	primal = 1,
	flux = 2
};


/** Data post-processor for the solution.
 * 
 * More precisely, for a numerical solution vector
 * \f$ \boldsymbol u_h \f$
 * the following solution data components are extracted
 * 
 * - primal \f$ u \f$,
 * - flux \f$ \boldsymbol q = -\epsilon \nabla u \f$
 * 
 * with post-processing operations for an global output.
 * 
 * Objects from this class can directly attached to the deal.II DataOut write
 * functions and to the DTM++.core DTM::DataOutput write function.
 */
template<int dim>
class DataPostprocessor : public dealii::DataPostprocessor<dim> {
public:
	DataPostprocessor(
		const unsigned int _output_quantities,
		std::shared_ptr< dealii::Function<dim> > _epsilon
 		) :
 		output_quantities(_output_quantities),
		uflags(
			dealii::update_values |
			dealii::update_gradients |
			dealii::update_quadrature_points
		) {
		function.epsilon = _epsilon;
		
		////////////////////////////////////////////////////////////////////////
		// set up postprocessed component names and dci
		
		unsigned int internal_component=0;
		
		if (output_quantities & OutputQuantities::primal) {
			postprocessed_quantities_names.push_back("primal");
			dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
			internal_primal_component = internal_component;
			++internal_component;
		}
		else {
			internal_primal_component = dealii::numbers::invalid_unsigned_int;
		}
		
		if (output_quantities & OutputQuantities::flux) {
			internal_first_flux_component = internal_component;
			
			for (unsigned int k(0); k < dim; ++k) {
				postprocessed_quantities_names.push_back("flux");
				dci.push_back(
					dealii::DataComponentInterpretation::component_is_part_of_vector
				);
				++internal_component;
			}
		}
		else {
			internal_first_flux_component = dealii::numbers::invalid_unsigned_int;
		}
		
		internal_n_components = internal_component;
		
		Assert(
			internal_n_components > 0,
			dealii::ExcMessage("You specified no output quantity")
		);
	}
	
	virtual ~DataPostprocessor() = default;
	
	virtual
	void
	evaluate_scalar_field(
		const dealii::DataPostprocessorInputs::Scalar<dim> &input_data,
		std::vector< dealii::Vector<double> > &postprocessed_quantities
	) const;
	
	virtual std::vector< std::string > get_names() const;
	
	virtual
	std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation >
	get_data_component_interpretation() const;
	
	
	virtual dealii::UpdateFlags get_needed_update_flags() const;
	
private:
	unsigned int output_quantities;
	
	unsigned int internal_primal_component;
	unsigned int internal_first_flux_component;
	
	unsigned int internal_n_components;
	
	std::vector< std::string > postprocessed_quantities_names;
	std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci;
	const dealii::UpdateFlags uflags;
	
	struct {
		std::shared_ptr< dealii::Function<dim> >
			epsilon; ///< Diffusion coefficient function.
	} function;
};

} // namespace

#endif
