/**
 * @file Force8.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-13, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <elasticity/Force/Force8.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace vectorvalued {

template <int dim>
Force8<dim>::
Force8(
	const double r1,
	dealii::Point<dim> &p1,
	const double r2,
	dealii::Point<dim> &p2,
	const double r3,
	dealii::Point<dim> &p3
) : dealii::Function<dim> (dim),
	r1(r1), p1(p1), r2(r2), p2(p2), r3(r3), p3(p3) {
	
}

template <int dim>
void Force8<dim>::vector_value (
	const dealii::Point<dim> &x,
	dealii::Vector<double> &y
) const {
	y.reinit(dim);
	y=0.;
	
	if ( ( (x-p1).norm_square() < r1*r1 ) || ( (x-p2).norm_square() < r2*r2) ) {
		y[0] = 1.;
	}
	
	if ( (dim > 1) && ( (x-p3).norm_square() < r3*r3 ) ) {
		y[1] = 1.;
	}
}


template <int dim>
double Force8<dim>::value (
	const dealii::Point<dim> &x,
	const unsigned int component
) const {
	dealii::Vector<double> y;
	y.reinit(dim);
	
	vector_value(x,y);
	return y(component);
}

} // namespace

#include "Force8.inst.in"
