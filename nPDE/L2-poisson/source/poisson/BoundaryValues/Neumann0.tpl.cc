/**
 * @file Neumann0.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-10-12, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <poisson/BoundaryValues/Neumann0.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace poisson {

template <int dim>
double Neumann0<dim>::value (
	const dealii::Point<dim> &x,
	const unsigned int /* component */
) const {
	// TODO: NOTE: solution
	double val = {0.};
	
	if ( std::abs( x(0)-1. ) < 1.e-14 ) {
		val = (2.*x(0)-1.)*x(1)*(x(1)-1.);
	}
	else {
		val = ((x(1)-.5) < 0 ? -1. : 1.) * (x(0)*(x(0)-1.)*(2.*x(1)-1.));
	}
	
	return val * function.c->value(x, 0);
}

} // namespace

#include "Neumann0.inst.in"
