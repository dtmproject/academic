/**
 * @file Poisson_cG_DWR.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-08-11, Poisson/DWR, UK
 * @date 2016-02-10, condiffrea/SUPG, UK
 * @date 2016-01-15, condiff/SUPG, UK
 * @date 2016-01-14, condiff, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief Poisson/DWR Problem
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Poisson/Poisson_cG_DWR.tpl.hh>
#include <Poisson/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/grid/grid_refinement.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/sparse_direct.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>


// C++ includes
#include <fstream>
#include <vector>

namespace Poisson {

template<int dim>
void
Poisson_cG_DWR<dim>::
set_grid(std::shared_ptr< Grid_DWR<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
Poisson_cG_DWR<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
Poisson_cG_DWR<dim>::
set_evaluation_point(dealii::Point<dim> evaluation_point) {
	dual.evaluation_point = evaluation_point;
}


template<int dim>
void
Poisson_cG_DWR<dim>::
set_data(
	const unsigned int p_primal,
	const unsigned int p_dual,
	const unsigned int global_refinement) {
	data.p_primal = p_primal;
	data.p_dual   = p_dual;
	data.global_refinement = global_refinement;
}


template<int dim>
void
Poisson_cG_DWR<dim>::
set_data_output_patches(
	unsigned int _data_output_patches_primal,
	unsigned int _data_output_patches_dual) {
	primal.data_output_patches = _data_output_patches_primal;
	dual.data_output_patches = _data_output_patches_dual;
}


template<int dim>
void
Poisson_cG_DWR<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	grid->primal.fe = std::make_shared< dealii::FE_Q<dim> > (data.p_primal);
	grid->dual.fe = std::make_shared< dealii::FE_Q<dim> > (data.p_dual);
	
	grid->primal.mapping = std::make_shared< dealii::MappingQ<dim> > (data.p_primal);
	grid->dual.mapping = std::make_shared< dealii::MappingQ<dim> > (data.p_dual);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	
	////////////////////////////////////////////////////////////////////////////
	// init error estimators
	//
	error_estimator.DWR = std::make_shared<
		Poisson::DWR::ErrorEstimator<dim> > ();
	error_estimator.DWR->set_objects(
		grid,
		function.f
	);
	
	////////////////////////////////////////////////////////////////////////////
	// INIT DATA OUTPUT
	//
	Assert(grid->primal.dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.dof.use_count(), dealii::ExcNotInitialized());
	
	DTM::pout << "Poisson DWR: primal solution data output: patches = " << primal.data_output_patches << std::endl;
	DTM::pout << "Poisson DWR: dual solution   data output: patches = " << dual.data_output_patches << std::endl;
	
	{
		std::vector<std::string> data_field_names;
		data_field_names.push_back("u");
		
		std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci_field;
		dci_field.push_back(dealii::DataComponentInterpretation::component_is_scalar);
		
		primal.data_output.set_DoF_data(
			grid->primal.dof
		);
		
		primal.data_output.set_data_field_names(data_field_names);
		primal.data_output.set_data_component_interpretation_field(dci_field);
		primal.data_output.set_data_output_patches(primal.data_output_patches);
	}
	
	{
		std::vector<std::string> data_field_names;
		data_field_names.push_back("u");
		data_field_names.push_back("z");
		
		std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci_field;
		dci_field.push_back(dealii::DataComponentInterpretation::component_is_scalar);
		dci_field.push_back(dealii::DataComponentInterpretation::component_is_scalar);
		
		dual.data_output.set_DoF_data(
			grid->dual.dof
		);
		
		dual.data_output.set_data_field_names(data_field_names);
		dual.data_output.set_data_component_interpretation_field(dci_field);
		dual.data_output.set_data_output_patches(dual.data_output_patches);
	}
}


template<int dim>
void
Poisson_cG_DWR<dim>::
reinit() {
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->primal.dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.sp.use_count(), dealii::ExcNotInitialized());
	
	primal.A.reinit(*(grid->primal.sp));
	primal.u = std::make_shared< dealii::Vector<double> > ();
	primal.u->reinit(grid->primal.dof->n_dofs());
	primal.f.reinit(grid->primal.dof->n_dofs());
	
	Assert(grid->dual.dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.sp.use_count(), dealii::ExcNotInitialized());
	
	dual.u = std::make_shared< dealii::Vector<double> > ();
	dual.u->reinit(grid->dual.dof->n_dofs());
	
	dual.A.reinit(*(grid->dual.sp));
	dual.z = std::make_shared< dealii::Vector<double> > ();
	dual.z->reinit(grid->dual.dof->n_dofs());
	dual.Je.reinit(grid->dual.dof->n_dofs());
	
	dual.solution_vectors.resize(0);
	dual.solution_vectors.push_back(dual.u);
	dual.solution_vectors.push_back(dual.z);
}


template<int dim>
void
Poisson_cG_DWR<dim>::
primal_assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.constraints.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	
	// Initialise the system matrix with 0.
	primal.A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points
	dealii::QGauss<dim> quad ( grid->primal.fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->primal.mapping),
		*(grid->primal.fe),
		quad,
		dealii::update_gradients | // update shape function gradient values
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_A(
		grid->primal.fe->dofs_per_cell, grid->primal.fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->primal.fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	auto cell = grid->primal.dof->begin_active();
	auto endc = grid->primal.dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix for the cell assembly with 0.
		local_A = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->primal.fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->primal.fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			// diffusion
			local_A(i,j) +=
				fe_values.shape_grad(i,q) *
				fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		Assert(
			(local_dof_indices.size() == grid->primal.fe->dofs_per_cell),
			dealii::ExcNotInitialized()
		);
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		Assert(grid->primal.constraints.use_count(), dealii::ExcNotInitialized());
		
		grid->primal.constraints->distribute_local_to_global(
			local_A, local_dof_indices, primal.A
		);
	}
}


template<int dim>
void
Poisson_cG_DWR<dim>::
primal_assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->primal.constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	primal.f = 0;
	function.f->set_time(0.);
	
	dealii::QGauss<dim> quad ( grid->primal.fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->primal.mapping),
		*(grid->primal.fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->primal.fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->primal.fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->primal.dof->begin_active();
	auto endc = grid->primal.dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->primal.fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->primal.constraints->distribute_local_to_global(
			local_f, local_dof_indices, primal.f
		);
	}
}


template<int dim>
void
Poisson_cG_DWR<dim>::
primal_solve() {
	////////////////////////////////////////////////////////////////////////////
	// apply Dirichlet boundary values
	std::map<dealii::types::global_dof_index, double> boundary_values;
	dealii::VectorTools::interpolate_boundary_values(
		*grid->primal.dof,
		static_cast< dealii::types::boundary_id > (
			Poisson::types::boundary_id::Dirichlet
		),
		dealii::ConstantFunction<dim> (0.0),
		boundary_values
	);
	
	dealii::MatrixTools::apply_boundary_values(
		boundary_values,
		primal.A,
		*primal.u,
		primal.f
	);
	
	////////////////////////////////////////////////////////////////////////////
	// solve linear system
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(primal.A);
	iA.vmult(*primal.u, primal.f);
	
	////////////////////////////////////////////////////////////////////////////
	// distribute hanging node constraints on solution
	grid->primal.constraints->distribute(*primal.u);
}


template<int dim>
void
Poisson_cG_DWR<dim>::
interpolate_primal_to_dual() {
	dealii::FETools::interpolate(
		*(grid->primal.dof),
		*(primal.u),
		*(grid->dual.dof),
		*(grid->dual.constraints),
		*(dual.u)
	);
}


template<int dim>
void
Poisson_cG_DWR<dim>::
dual_assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.constraints.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	
	// Initialise the system matrix with 0.
	dual.A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points
	dealii::QGauss<dim> quad ( grid->dual.fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->dual.mapping),
		*(grid->dual.fe),
		quad,
		dealii::update_gradients | // update shape function gradient values
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_A(
		grid->dual.fe->dofs_per_cell, grid->dual.fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->dual.fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	auto cell = grid->dual.dof->begin_active();
	auto endc = grid->dual.dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix for the cell assembly with 0.
		local_A = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->dual.fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->dual.fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			// diffusion
			local_A(i,j) +=
				fe_values.shape_grad(i,q) *
				fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		Assert(
			(local_dof_indices.size() == grid->dual.fe->dofs_per_cell),
			dealii::ExcNotInitialized()
		);
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		Assert(grid->dual.constraints.use_count(), dealii::ExcNotInitialized());
		
		grid->dual.constraints->distribute_local_to_global(
			local_A, local_dof_indices, dual.A
		);
	}
}


template<int dim>
void
Poisson_cG_DWR<dim>::
dual_assemble_Je() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dual.constraints.use_count(), dealii::ExcNotInitialized());
	
	dual.Je = 0;
	
	auto cell = grid->dual.dof->begin_active();
	auto endc = grid->dual.dof->end();
	
	for ( ; cell != endc; ++cell) {
	for (unsigned int vertex=0;
		vertex<dealii::GeometryInfo<dim>::vertices_per_cell;
		++vertex) {
		if (cell->vertex(vertex).distance(dual.evaluation_point) < cell->diameter()*1e-8) {
			dual.Je(cell->vertex_dof_index(vertex,0)) = 1;
			return;
		}
	}}
}


template<int dim>
void
Poisson_cG_DWR<dim>::
dual_solve() {
	////////////////////////////////////////////////////////////////////////////
	// apply Dirichlet boundary values
	std::map<dealii::types::global_dof_index, double> boundary_values;
	dealii::VectorTools::interpolate_boundary_values(
		*grid->dual.dof,
		static_cast< dealii::types::boundary_id > (
			Poisson::types::boundary_id::Dirichlet
		),
		dealii::ZeroFunction<dim> (),
		boundary_values
	);
	
	dealii::MatrixTools::apply_boundary_values(
		boundary_values,
		dual.A,
		*dual.z,
		dual.Je
	);
	
	////////////////////////////////////////////////////////////////////////////
	// solve linear system
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(dual.A);
	iA.vmult(*dual.z, dual.Je);
	
	////////////////////////////////////////////////////////////////////////////
	// distribute hanging node constraints on solution
	grid->dual.constraints->distribute(*dual.z);
}


template<int dim>
void
Poisson_cG_DWR<dim>::
do_data_output(const double cycle) {
	const double solution_time = cycle;
	
	primal.data_output.write_data("primal", primal.u, solution_time);
	dual.data_output.write_data("dual", dual.solution_vectors, solution_time);
}


template<int dim>
void
Poisson_cG_DWR<dim>::
refine_grid() {
	auto error_indicators = std::make_shared< dealii::Vector<double> > ();
	error_indicators->reinit(grid->tria->n_active_cells());
	
	error_estimator.DWR->estimate(
		dual.u,
		dual.z,
		error_indicators
	);
	
	for (unsigned int i=0; i < error_indicators->size(); ++i) {
		(*error_indicators)[i] = std::fabs((*error_indicators)[i]);
	}
	
	dealii::GridRefinement::refine_and_coarsen_fixed_fraction(
		*(grid->tria),
		*(error_indicators),
		0.8, 0.02
	);
	
	grid->tria->execute_coarsening_and_refinement();
}


template<int dim>
void
Poisson_cG_DWR<dim>::
run() {
	init(data.global_refinement);
	
	for (unsigned int cycle = 0; cycle < 8; ++cycle) {
		reinit();
		
		primal_assemble_system();
		primal_assemble_f();
		primal_solve();
		
		interpolate_primal_to_dual();
		
		dual_assemble_system();
		dual_assemble_Je();
		dual_solve();
		
		// write out the solution
		do_data_output(cycle);
		
		refine_grid();
	}
}

} // namespace

#include "Poisson_cG_DWR.inst.in"
