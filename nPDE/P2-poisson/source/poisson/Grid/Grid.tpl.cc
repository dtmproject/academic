/**
 * @file Grid.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2021-10-20, UK
 * @date 2016-10-12, UK
 * @date 2015-11-11, UK
 * @date 2015-05-15, DTM++/AcousticWave Module, UK
 * @date (2012-07-26), 2013-08-15, ElasticWave, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */


// PROJECT includes
#include <poisson/Grid/Grid.tpl.hh>

// DEAL.II includes
#include <deal.II/lac/sparsity_pattern.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

// class declaration
namespace poisson {

/// Constructor.
template<int dim, int spacedim>
Grid<dim,spacedim>::
Grid() :
	tria(std::make_shared< dealii::Triangulation<dim> >(
		typename dealii::Triangulation<dim>::MeshSmoothing(
			dealii::Triangulation<dim>::smoothing_on_refinement |
			dealii::Triangulation<dim>::smoothing_on_coarsening)
	)),
	dof( std::make_shared< dealii::DoFHandler<dim> > (*tria) ),
	constraints( std::make_shared< dealii::AffineConstraints<double> > () ),
	sp( std::make_shared< dealii::SparsityPattern >() ) {
}


/// Destructor. Clears DoFHandler.
template<int dim, int spacedim>
Grid<dim,spacedim>::
~Grid() {
	dof->clear();
}


/// Generate grid. Throws Exception in base class.
template<int dim, int spacedim>
void
Grid<dim,spacedim>::
generate() {
	AssertThrow(false,
		dealii::ExcMessage("Grid::generate of base was called,"
		"here is nothing implemented!"));
}


/// Global refinement.
template<int dim, int spacedim>
void
Grid<dim,spacedim>::
refine_global(const unsigned int n) {
	tria->refine_global(n);
}


/// Set boundary indicators
template<int dim, int spacedim>
void
Grid<dim,spacedim>::
set_boundary_indicators() {
	AssertThrow(false, dealii::ExcNotImplemented());
}


/// Distribute.
template<int dim, int spacedim>
void
Grid<dim,spacedim>::
distribute() {
	// Distribute the degrees of freedom (dofs)
	Assert(dof.use_count(), dealii::ExcNotInitialized());
	Assert(fe.use_count(), dealii::ExcNotInitialized());
	dof->distribute_dofs(*fe);
	
	// setup constraints like boundary values or hanging nodes
	Assert(constraints.use_count(), dealii::ExcNotInitialized());
	constraints->clear();
	constraints->reinit();
	
	dealii::DoFTools::make_hanging_node_constraints(*dof, *constraints);
	
	constraints->close();
	
	// Now we create a sparsity pattern, which we will use to initialise
	// our system matrix (for the assembly step).
	// See deal.II step-2 tutorial for details.
	dealii::DynamicSparsityPattern dsp(dof->n_dofs(), dof->n_dofs());
	dealii::DoFTools::make_sparsity_pattern(*dof, dsp, *constraints, false);
	Assert(sp.use_count(), dealii::ExcNotInitialized());
	sp->copy_from(dsp);
}

} // namespaces

#include "Grid.inst.in"
