/**
 * @file sigma0_MovingHump.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2017-02-01, UK
 */

/*  Copyright (C) 2012-2017 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __sigma0_MovingHump_tpl_hh
#define __sigma0_MovingHump_tpl_hh

// PROJECT includes
#include <DTM++/base/Function.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>

// C++ includes

namespace condiff {

template<int dim>
class sigma0_MovingHump : public DTM::Function<dim,1> {
public:
	sigma0_MovingHump(
		const double epsilon_high,
		const double epsilon_low
	);
	
	virtual ~sigma0_MovingHump() = default;
	
	virtual double value (
		const dealii::Point<dim> &x,
		const unsigned int component = 0
	) const;
	
private:
	double epsilon_high;
	double epsilon_low;
};

} // namespace

#endif
