/**
 * @file Neumann2.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-11-11, UK
 * @date 2015-11-09, UK
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <Poisson/Neumann/Neumann2.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace Poisson {

template <int dim>
double Neumann2<dim>::value (
	const dealii::Point<dim> &x,
	const unsigned int /* component */
) const {
	Assert(dim==2, dealii::ExcNotImplemented());
	
	return -x(1)*(x(1)-1.);
}

} // namespace

#include "Neumann2.inst.in"
