/**
 * @file Poisson_cG.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-22, modifications for Dirichlet, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief Poisson Problem
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Poisson/Poisson_cG.tpl.hh>
#include <Poisson/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/numerics/data_out.h>

// new: (interpolate boundary values, apply boundary values and direct solver)
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/lac/sparse_direct.h>

// new: (grid refinement, error estimators)
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/numerics/error_estimator.h>

// C++ includes
#include <fstream>
#include <string>
#include <vector>

namespace Poisson {

template<int dim>
Poisson_cG<dim>::
Poisson_cG(const unsigned int p) : p(p) {
}


template<int dim>
void
Poisson_cG<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
Poisson_cG<dim>::
set_c(std::shared_ptr< dealii::Function<dim> > c) {
	function.c = c;
}


template<int dim>
void
Poisson_cG<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
Poisson_cG<dim>::
set_g(std::shared_ptr< dealii::Function<dim> > g) {
	function.g = g;
}


template<int dim>
void
Poisson_cG<dim>::
set_h(std::shared_ptr< dealii::Function<dim> > h) {
	function.h = h;
}


template<int dim>
void
Poisson_cG<dim>::
run(const unsigned int global_refinement) {
	init(global_refinement);
	
	for (unsigned refinement_cycle=0; refinement_cycle < 6; ++refinement_cycle) { 
		if (refinement_cycle) {
			estimate_error_and_mark();
			reinit();
		}
		
		assemble_system();
		assemble_f();
		assemble_grad_h();
		
		solve(refinement_cycle);
	}
}


template<int dim>
void
Poisson_cG<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q-GLL finite element and boundary mapping
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	dealii::QGaussLobatto<1> support_points(p+1);
	grid->fe = std::make_shared< dealii::FE_Q<dim> > (support_points);
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u.reinit(grid->dof->n_dofs());
	b.reinit(grid->dof->n_dofs());
	f.reinit(grid->dof->n_dofs());
	h.reinit(grid->dof->n_dofs());
	
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	A.reinit(*(grid->sp));
}

template<int dim>
void
Poisson_cG<dim>::
estimate_error_and_mark() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->tria.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(u.size(), dealii::ExcNotInitialized());
	
	dealii::Vector<float> estimated_error_per_cell;
	estimated_error_per_cell.reinit(grid->tria->n_active_cells());
	
	dealii::QGauss<dim-1> quad_face(grid->fe->degree + 1);
	
	// estimate errors
	dealii::KellyErrorEstimator<dim>::estimate(
		*grid->dof,
		quad_face,
		{}, // TODO: map for Neumann bnd
		u, // solution vector
		estimated_error_per_cell
		// TODO other inputs
	);
	
	// mark cells for refinement
	dealii::GridRefinement::refine_and_coarsen_fixed_fraction(
		*grid->tria,
		estimated_error_per_cell,
		0.8, // top fraction for refinement
		0.0 // bottom fraction for coarsening
		// TODO: max. no. of cells after refinement
	);
	
}

template<int dim>
void
Poisson_cG<dim>::
reinit() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	// reinit tria of grid and distribute dofs
	grid->tria->execute_coarsening_and_refinement();
	grid->set_boundary_indicators();
	
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u.reinit(grid->dof->n_dofs());
	b.reinit(grid->dof->n_dofs());
	f.reinit(grid->dof->n_dofs());
	h.reinit(grid->dof->n_dofs());
	
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	A.reinit(*(grid->sp));
}

template<int dim>
void
Poisson_cG<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	Assert(function.c.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	// NOTE: see deal.II step-3 tutorial for details
	// Here, we want to assemble the system matrix of the weak Laplacian operator.
	// Due to FEM, this can be done by summing up local contributions over
	// all cells and trial/test function combinations.
	// The inner most integral will be solved through a numerical approximation
	// by a Gaussian quadrature rule.
	
	// Initialise the system matrix with 0.
	A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_gradients |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_A(
		grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standard template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix for the cell assembly with 0.
		local_A = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_A(i,j) +=
				fe_values.shape_grad(i,q) *
				function.c->value(fe_values.quadrature_point(q), 0) *
				fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_A,
			local_dof_indices, // TODO: new for adaptive refinement!!!
			A
		);
	}
}


template<int dim>
void
Poisson_cG<dim>::
assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	f = 0;
	
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standard template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_f, local_dof_indices, f
		);
	}
}


template<int dim>
void
Poisson_cG<dim>::
assemble_grad_h() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.h.use_count(), dealii::ExcNotInitialized());
	h = 0;
	
	dealii::QGaussLobatto<dim-1> quad_face ( grid->fe->tensor_degree()+1 );
	
	dealii::FEFaceValues<dim> fe_face_values(
		*(grid->mapping),
		*(grid->fe),
		quad_face,
		dealii::update_values |
		dealii::update_normal_vectors |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_h(
		grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standard template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell)
	if (cell->at_boundary()) {
		// init local assemblies vector
		local_h = 0;
		
		// loop over all faces
		for (unsigned int face_no(0);
			face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
			// store iterator on the current face
			auto face = cell->face(face_no);
			
			// integrate on the Neumann boundary part only
			if (face->at_boundary() &&
				(face->boundary_id() ==
				static_cast<dealii::types::boundary_id>(Poisson::types::boundary_id::Neumann))
			) {
				// reinit fe_face_values since we have to integrate on this face
				fe_face_values.reinit(cell,face_no);
				
				// Now loop over all shape function combinations and quadrature points
				// to get the assembly.
				for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
				for (unsigned int q(0); q < quad_face.size(); ++q) {
					local_h(i) +=
						fe_face_values.shape_value(i,q) *
// 						function.h->value(fe_face_values.quadrature_point(q), 0) *
						function.h->gradient(fe_face_values.quadrature_point(q)) *
						fe_face_values.normal_vector(q) *
						fe_face_values.JxW(q);
				}
			} // Neumann face
		} // loop over all faces of a cell
		
		// Get the global indices into the vector local_dof_indices.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assemblies to the global matrix.
		grid->constraints->distribute_local_to_global(
			local_h, local_dof_indices, h
		);
	} // loop over all cells (at boundary)
}


template<int dim>
void
Poisson_cG<dim>::
solve(const unsigned int refinement_cycle) {
	b = 0;
	b.add(1., f);
	b.add(1., h);
	
	// fetch Dirichlet boundary values for the Dirichlet-marked dofs by interpolation:
	std::map<dealii::types::global_dof_index, double> boundary_values;
	
	Assert(function.g.use_count(), dealii::ExcNotInitialized());
	function.g->set_time(0.);
	
	dealii::VectorTools::interpolate_boundary_values(
		*grid->dof,
		static_cast< dealii::types::boundary_id > (
			Poisson::types::boundary_id::Dirichlet
		),
		*function.g,
		boundary_values // final object
	);
	
	// apply Dirichlet boundary values to the system:
	dealii::MatrixTools::apply_boundary_values(
		boundary_values,
		A, u, b
	);
	
	// solve the linear system Au = b
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(A);
	iA.vmult(u, b); // solve linear system
	
	grid->constraints->distribute(u); // TODO: new
	
	write_vtk("u", u, refinement_cycle);
}


template<int dim>
void
Poisson_cG<dim>::
write_vtk(
	std::string name,
	dealii::Vector<double> &solution_vector,
	const unsigned int refinement_cycle) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	
	std::ofstream output_file(
		(name+"-"+std::to_string(refinement_cycle)+".vtk").c_str()
	);
	
	if (p == 1) {
		dealii::DataOut<dim> data_out;
		data_out.attach_dof_handler(*(grid->dof));
		data_out.add_data_vector(solution_vector, name);
		data_out.build_patches(grid->fe->tensor_degree());
		
		data_out.write_vtk(output_file);
	}
	else {
		// prepare second triangulation with same fe discretization
		dealii::Triangulation<dim> tria2;
		tria2.copy_triangulation(*grid->tria);
		tria2.refine_global(p-1);
		
		dealii::DoFHandler<dim> dof2(tria2);
		dof2.distribute_dofs(*grid->fe);
		
		dealii::Vector<double> interpolated_solution_vector;
		interpolated_solution_vector.reinit(dof2.n_dofs());
		
		dealii::AffineConstraints<double> constraints2;
		constraints2.clear();
		constraints2.reinit();
		dealii::DoFTools::make_hanging_node_constraints(dof2, constraints2);
		constraints2.close();
		
		// interpolation to the refined triangulation
		dealii::VectorTools::interpolate_to_different_mesh(
			// computational mesh
			*grid->dof,
			solution_vector,
			// new mesh
			dof2,
			constraints2,
			interpolated_solution_vector
		);
		
		// apply the Dirichlet conditions again strongly
		std::map<dealii::types::global_dof_index, double> boundary_values;
		
		Assert(function.g.use_count(), dealii::ExcNotInitialized());
		function.g->set_time(0.);
		
		dealii::VectorTools::interpolate_boundary_values(
			dof2,
			static_cast< dealii::types::boundary_id > (
				Poisson::types::boundary_id::Dirichlet
			),
			*function.g,
			boundary_values // final object
		);
		
		for ( auto &boundary_value_pair : boundary_values) {
			interpolated_solution_vector[boundary_value_pair.first] = boundary_value_pair.second;
		}
		
		// data output
		dealii::DataOut<dim> data_out;
		data_out.attach_dof_handler(dof2);
		data_out.add_data_vector(interpolated_solution_vector, name);
		data_out.build_patches(1);
		
		data_out.write_vtk(output_file);
		
		dof2.clear();
	}
}

} // namespace

#include "Poisson_cG.inst.in"
