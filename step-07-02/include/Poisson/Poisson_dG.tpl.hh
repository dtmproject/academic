/**
 * @file Poisson_dG.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-08-31, Poisson dG, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Poisson_dG_tpl_hh
#define __Poisson_dG_tpl_hh

// PROJECT includes
#include <Poisson/Grid/Grid.tpl.hh>

// DTM++ includes

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace Poisson {

template<int dim>
class Poisson_dG {
public:
	Poisson_dG() = default;
	virtual ~Poisson_dG() = default;
	
	virtual void set_grid(std::shared_ptr< Poisson::Grid<dim,1> > grid);
	
	virtual void set_epsilon(std::shared_ptr< dealii::Function<dim> > epsilon);
	
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	
	virtual void set_u_D(std::shared_ptr< dealii::Function<dim> > u_D);
	
	virtual void set_data(
		const unsigned int p,
		const double S,
		const unsigned int global_refinement
	);
	
	virtual void run();

protected:
	virtual void init(const unsigned int global_refinement);
	virtual void reinit();
	
	// primal problem
	virtual void assemble_system();
	virtual void assemble_f();
	virtual void assemble_u_D();
	virtual void solve();
	
	virtual void do_data_output();
	
	dealii::SparseMatrix<double> A; ///< primal problem system matrix
	std::shared_ptr< dealii::Vector<double> > u; ///< primal problem solution
	dealii::Vector<double> b;       ///< primal problem rhs
	
	dealii::Vector<double> f;
	dealii::Vector<double> u_D;
	
	double penalty;
	
	std::shared_ptr< Poisson::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > epsilon; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > u_D; ///< Dirichlet boundary function.
	} function;
	
	struct {
		unsigned int p;
		double S;
		unsigned int global_refinement;
	} data;
};

} // namespace

#endif
