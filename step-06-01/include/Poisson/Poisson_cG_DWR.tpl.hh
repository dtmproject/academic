/**
 * @file Poisson_cG_DWR.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-08-11, Poisson / DWR, UK
 * @date 2016-02-10, linear reaction, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief Poisson Problem
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Poisson_cG_DWR_tpl_hh
#define __Poisson_cG_DWR_tpl_hh

// PROJECT includes
#include <Poisson/Grid/Grid_DWR.tpl.hh>
#include <Poisson/ErrorEstimator/ErrorEstimators.hh>

// DTM++ includes
#include <DTM++/io/DataOutput.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/fe/fe.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace Poisson {

template<int dim>
class Poisson_cG_DWR {
public:
	Poisson_cG_DWR() = default;
	virtual ~Poisson_cG_DWR() = default;
	
	virtual void set_grid(std::shared_ptr< Poisson::Grid_DWR<dim,1> > grid);
	
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	
	virtual void set_evaluation_point(dealii::Point<dim> evaluation_point);
	
	virtual void set_data(
		const unsigned int p_primal,
		const unsigned int p_dual,
		const unsigned int global_refinement
	);
	
	virtual void set_data_output_patches(
		unsigned int data_output_patches_primal,
		unsigned int data_output_patches_dual
	);
	
	virtual void run();

protected:
	virtual void init(const unsigned int global_refinement);
	virtual void reinit();
	
	// primal problem
	virtual void primal_assemble_system();
	virtual void primal_assemble_f();
	virtual void primal_solve();
	
	virtual void interpolate_primal_to_dual();
	
	// dual problem
	virtual void dual_assemble_system();
	virtual void dual_assemble_Je();
	virtual void dual_solve();
	
	virtual void do_data_output(const double cycle);
	
	virtual void refine_grid();
	
	struct {
		dealii::SparseMatrix<double> A; ///< primal problem system matrix
		std::shared_ptr< dealii::Vector<double> > u; ///< primal problem solution
		dealii::Vector<double> f;       ///< primal problem rhs
		
		// Data Output
		DTM::DataOutput<dim> data_output;
		unsigned int data_output_patches;
	} primal;
	
	struct {
		std::shared_ptr< dealii::Vector<double> > u; ///< primal problem solution on dual space
		
		dealii::SparseMatrix<double> A;              ///< dual problem system matrix
		std::shared_ptr< dealii::Vector<double> > z; ///< dual problem solution
		dealii::Vector<double> Je;                   ///< dual problem rhs
		dealii::Point<dim> evaluation_point;
		
		// Data Output
		DTM::DataOutput<dim> data_output;
		unsigned int data_output_patches;
		std::vector< std::shared_ptr< dealii::Vector<double> > > solution_vectors;
	} dual;
	
	std::shared_ptr< Poisson::Grid_DWR<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
	} function;
	
	struct {
		std::shared_ptr< Poisson::DWR::ErrorEstimator<dim> > DWR;
	} error_estimator;
	
	
	struct {
		unsigned int p_primal; ///< polynomial degree p of primal problem
		unsigned int p_dual; ///< polynomial degree p of dual problem
		unsigned int global_refinement;
	} data;
};

} // namespace

#endif
