/**
 * @file Poisson_dG.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-08-26, Poisson dG, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Poisson/Poisson_dG.tpl.hh>
#include <Poisson/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_tools.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/grid/grid_refinement.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/sparse_direct.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>


// C++ includes
#include <fstream>
#include <vector>

namespace Poisson {

template<int dim>
void
Poisson_dG<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
Poisson_dG<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
Poisson_dG<dim>::
set_data(
	const unsigned int p,
	const double S,
	const unsigned int global_refinement) {
	data.p = p;
	data.S = S;
	data.global_refinement = global_refinement;
}


template<int dim>
void
Poisson_dG<dim>::
init(const unsigned int global_refinement) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	
	penalty = 100;
	
	dealii::QGauss<1> support_points(data.p+1);
// 	dealii::QGaussLobatto<1> support_points(data.p+1);
	grid->fe = std::make_shared< dealii::FE_DGQArbitraryNodes<dim> > (support_points);
	
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (data.p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	
}


template<int dim>
void
Poisson_dG<dim>::
reinit() {
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	
	A.reinit(*(grid->sp));
	u = std::make_shared< dealii::Vector<double> > ();
	u->reinit(grid->dof->n_dofs());
	f.reinit(grid->dof->n_dofs());
}


template<int dim>
void
Poisson_dG<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	
	// Initialise the system matrix with 0.
	A = 0;
	
	{
		// Setup a Gaussian quadrature formula
		dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
		
		// Setup a FE_Values object.
		// This is needed to get the needed information from the FiniteElement
		dealii::FEValues<dim> fe_values(
			*(grid->mapping),
			*(grid->fe),
			quad,
			dealii::update_gradients | // update shape function gradient values
			dealii::update_JxW_values
		);
		
		// Setup a (small) full matrix, to store the assembly on each mesh cell
		// efficiently.
		// Afterwards they will be distributed into the global (sparse) matrix A.
		dealii::FullMatrix<double> local_A(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		// Setup a small vector, to store the global dof indices.
		// NOTE: We are using a C++ standart template vector, not a deal.II
		// "Vector".
		// The FiniteElement object "fe" will tell us, how much dofs has each cell.
		std::vector< dealii::types::global_dof_index > local_dof_indices(
			grid->fe->dofs_per_cell
		);
		
		// Now we do the real work, looping over all cells to compute the cell
		// assemblys. For this we need an iterator (kind of a pointer), which allows
		// us, to iterate over each cell easily.
		// We initialise it with the first active cell of our triangulation.
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		for ( ; cell != endc; ++cell) {
			// First we have to compute the values of the gradients and
			// the JxW values.
			// The reinit of the fe_values object on the current cell will do this.
			fe_values.reinit(cell);
			
			// Initialise the full matrix for the cell assembly with 0.
			local_A = 0;
			
			// Now loop over all shape function combinations and quadrature points
			// to get the assembly.
			for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
			for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
			for (unsigned int q(0); q < quad.size(); ++q) {
				// diffusion
				local_A(i,j) +=
					fe_values.shape_grad(i,q) *
					fe_values.shape_grad(j,q) *
					fe_values.JxW(q);
			}
			
			// Store the global indices into the vector local_dof_indices.
			// The cell object will give us the information.
			Assert(
				(local_dof_indices.size() == grid->fe->dofs_per_cell),
				dealii::ExcNotInitialized()
			);
			cell->get_dof_indices(local_dof_indices);
			
			// Copy the local assembly to the global matrix.
			// We use the constraint object, to set all constraints with that step.
			Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
			
			grid->constraints->distribute_local_to_global(
				local_A, local_dof_indices, A
			);
		}
	} // cell volume assemblies
	
	{
		// assemble interior penalties
		AssertThrow((dim==1), dealii::ExcNotImplemented());
		
		// to get the values and gradients on the interior and boundary nodes,
		// we use a special "quadrature rule" which has only both cell ends
		dealii::QGaussLobatto<1> face_nodes ( 2 );
		
		dealii::FEValues<dim> fe_values(
			*(grid->mapping),
			*(grid->fe),
			face_nodes,
			dealii::update_values |
			dealii::update_gradients
		);
		
		dealii::FEValues<dim> fe_values_neighbor(
			*(grid->mapping),
			*(grid->fe),
			face_nodes,
			dealii::update_values |
			dealii::update_gradients
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices(
			grid->fe->dofs_per_cell
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices_neighbor(
			grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ui_vi(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ue_vi(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ui_ve(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		dealii::FullMatrix<double> local_ue_ve(
			grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
		);
		
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		for ( ; cell != endc; ++cell) {
		for (unsigned int face_no=0; face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
		if (cell->face(face_no)->at_boundary()) {
			// boundary face
			local_ui_vi = 0;
			
			fe_values.reinit(cell);
			cell->get_dof_indices(local_dof_indices);
			
			// get normal direction
			double n = (face_no == 0) ? -1 : 1;
			
			for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
			for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
				local_ui_vi(i,j) +=
					penalty * fe_values.shape_value(i,face_no) *
					fe_values.shape_value(j,face_no)
				-	fe_values.shape_value(i,face_no) *
					n * fe_values.shape_grad(j,face_no)[0]
				-	data.S * n * fe_values.shape_grad(i,face_no)[0] *
					fe_values.shape_value(j,face_no)
				;
			}}
			
			// copy assembly result
			grid->constraints->distribute_local_to_global(
				local_ui_vi, local_dof_indices, local_dof_indices, A
			);
		}
		else {
			// interior face
			local_ui_vi = 0;
			local_ue_vi = 0;
			local_ui_ve = 0;
			local_ue_ve = 0;
			
			// We assemble all interior face nodes only once from the
			// cell with the smaller index.
			// The assembly signs are adapted to their correct normal
			// direction: n = +1 and n_neighbor = -1
			
			auto neighbor = cell->neighbor(face_no);
			if (cell->index() < neighbor->index()) {
				// get normal direction
				double n          = (face_no == 0) ? -1 :  1;
				double n_neighbor = (face_no == 0) ?  1 : -1;
				
				fe_values.reinit(cell);
				cell->get_dof_indices(local_dof_indices);
				
				fe_values_neighbor.reinit(neighbor);
				neighbor->get_dof_indices(local_dof_indices_neighbor);
				
				// assemble interior penalties
				for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
					local_ui_vi(i,j) +=
						penalty * fe_values.shape_value(i,1) *
						fe_values.shape_value(j,1)
					-	.5*fe_values.shape_value(i,1) *
						n * fe_values.shape_grad(j,1)[0]
					-	.5*data.S * n * fe_values.shape_grad(i,1)[0] *
						fe_values.shape_value(j,1)
					;
				}}
				
				for (unsigned int i=0; i < fe_values.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values_neighbor.get_fe().dofs_per_cell; ++j) {
					local_ue_vi(i,j) +=
					-	penalty * fe_values.shape_value(i,1) *
						fe_values_neighbor.shape_value(j,0)
					+	.5*fe_values.shape_value(i,1) *
						n_neighbor * fe_values_neighbor.shape_grad(j,0)[0]
					+	.5*data.S *
						n * fe_values.shape_grad(i,1)[0] *
						fe_values_neighbor.shape_value(j,0)
					;
				}}
				
				for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values.get_fe().dofs_per_cell; ++j) {
					local_ui_ve(i,j) +=
					-	penalty * fe_values_neighbor.shape_value(i,0) *
						fe_values.shape_value(j,1)
					+	.5*fe_values_neighbor.shape_value(i,0) *
						n * fe_values.shape_grad(j,1)[0]
					+	.5*data.S *
						n_neighbor * fe_values_neighbor.shape_grad(i,0)[0] *
						fe_values.shape_value(j,1)
					;
				}}
				
				for (unsigned int i=0; i < fe_values_neighbor.get_fe().dofs_per_cell; ++i) {
				for (unsigned int j=0; j < fe_values_neighbor.get_fe().dofs_per_cell; ++j) {
					local_ue_ve(i,j) +=
						penalty * fe_values_neighbor.shape_value(i,0) *
						fe_values_neighbor.shape_value(j,0)
					-	.5*fe_values_neighbor.shape_value(i,0) *
						n_neighbor * fe_values_neighbor.shape_grad(j,0)[0]
					-	.5*data.S *
						n_neighbor * fe_values_neighbor.shape_grad(i,0)[0] *
						fe_values_neighbor.shape_value(j,0)
					;
				}}
				
				// copy assembly results
				grid->constraints->distribute_local_to_global(
					local_ui_vi, local_dof_indices, local_dof_indices, A
				);
				
				grid->constraints->distribute_local_to_global(
					local_ue_vi, local_dof_indices, local_dof_indices_neighbor, A
				);
				
				grid->constraints->distribute_local_to_global(
					local_ui_ve, local_dof_indices_neighbor, local_dof_indices, A
				);
				
				grid->constraints->distribute_local_to_global(
					local_ue_ve, local_dof_indices_neighbor, local_dof_indices_neighbor, A
				);
			}
		}}}
	} // interior and boundary assemblies
}


template<int dim>
void
Poisson_dG<dim>::
assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	f = 0;
	function.f->set_time(0.);
	
	dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_f, local_dof_indices, f
		);
	}
}


template<int dim>
void
Poisson_dG<dim>::
solve() {
	////////////////////////////////////////////////////////////////////////////
	// solve linear system
	dealii::SparseDirectUMFPACK iA;
	iA.initialize(A);
	iA.vmult(*u, f);
}


template<int dim>
void
Poisson_dG<dim>::
do_data_output() {
	{
		////////////////////////////////////////////////////////////////////////////
		// write solution nodal values to file
		
		dealii::QGaussLobatto<dim> quad ( 2+grid->fe->tensor_degree()+1 );
// 		dealii::QGauss<dim> quad ( grid->fe->tensor_degree()+1 );
		
		dealii::FEValues<dim> fe_values(
			*(grid->mapping),
			*(grid->fe),
			quad,
			dealii::update_values |
			dealii::update_quadrature_points
		);
		
		std::vector< dealii::types::global_dof_index > local_dof_indices(
			grid->fe->dofs_per_cell
		);
		
		std::map<unsigned int, std::pair< dealii::Point<dim>, double > > nodal_values;
		
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		unsigned int point_no = {1};
		for ( ; cell != endc; ++cell) {
			cell->get_dof_indices(local_dof_indices);
			fe_values.reinit(cell);
			
			for (unsigned int q=0; q < fe_values.get_quadrature().size(); ++q) {
				double value = {0};
				
				for (unsigned int j={0}; j < fe_values.get_fe().dofs_per_cell; ++j) {
					value += (*u)[local_dof_indices[j]] * fe_values.shape_value(j,q);
				}
				
				nodal_values.insert(
					std::make_pair(
						point_no++,
						std::make_pair(
							fe_values.quadrature_point(q),
							value
						)
					)
				);
			}
		}
		
		std::ofstream output_file(
			static_cast< std::string >("u.log").c_str()
		);
		
		for (unsigned int d=0; d < dim; ++d) {
			output_file << "x" << (d+1) << " ";
		}
		
		output_file << "u" << std::endl;
		for (const auto &nodal_value : nodal_values) {
			for (unsigned int d=0; d < dim; ++d) {
				output_file << nodal_value.second.first[d] << " ";
			}
			
			output_file << nodal_value.second.second << std::endl;
		}
		
		output_file.close();
	}
	
	{
		////////////////////////////////////////////////////////////////////////////
// 		std::ofstream output_file(
// 			(static_cast< std::string > ("u.vtk")).c_str()
// 		);
		
// 		dealii::DataOut<dim> data_out;
// 		data_out.attach_dof_handler(*(grid->dof));
// 		data_out.add_data_vector(*u, "u");
// 		data_out.build_patches(grid->fe->tensor_degree() ? grid->fe->tensor_degree() : 1);
		
// 		data_out.write_vtk(output_file);
	}
}


template<int dim>
void
Poisson_dG<dim>::
run() {
	init(data.global_refinement);
	reinit();
	
	assemble_system();
	assemble_f();
	
	solve();
	
	// write out the solution
	do_data_output();
	
}

} // namespace

#include "Poisson_dG.inst.in"
