/**
 * @file Poisson_cG.tpl.hh
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-22, modifications for Dirichlet, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief Poisson Problem
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Poisson_cG_tpl_hh
#define __Poisson_cG_tpl_hh

// PROJECT includes
#include <Poisson/Grid/Grid.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>

#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace Poisson {

/** Poisson_cG class.
 * @brief Handle Poisson's PDE problem with p-FEM (cG(p)) using GLL-elements.
 */
template<int dim>
class Poisson_cG {
public:
	Poisson_cG(const unsigned int p);
	virtual ~Poisson_cG() = default;
	
	virtual void set_grid(std::shared_ptr< Poisson::Grid<dim,1> > grid);
	
	virtual void set_c(std::shared_ptr< dealii::Function<dim> > c);
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	virtual void set_g(std::shared_ptr< dealii::Function<dim> > g);
	virtual void set_h(std::shared_ptr< dealii::Function<dim> > h);
	
	virtual void run(const unsigned int global_refinement);

protected:
	virtual void init(const unsigned int global_refinement);
	
	virtual void assemble_system();
	virtual void assemble_f();
	virtual void assemble_grad_h();
	
	virtual void solve();
	
	virtual void write_vtk(std::string name, dealii::Vector<double> &);
	
	dealii::SparseMatrix<double> A;    ///< System matrix.
	dealii::Vector<double> u;          ///< Solution vector.
	dealii::Vector<double> b;          ///< Right hand side vector.
	
	dealii::Vector<double> f;          ///< Forcing terms vector.
	dealii::Vector<double> h;          ///< Neumann boundary terms vector.
	
	std::shared_ptr< Poisson::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > c; ///< Diffusion coefficient function.
		
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > g; ///< Dirichlet boundary function.
		std::shared_ptr< dealii::Function<dim> > h; ///< Neumann boundary function.
	} function;
	
	const unsigned int p; ///< polynomial degree p
};

} // namespace

#endif
