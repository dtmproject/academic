/**
 * @file arclength.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-30, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Arclength/arclength.tpl.hh>
#include <Arclength/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>

// C++ includes
#include <fstream>
#include <vector>

namespace Arclength {

template<int dim>
arclength<dim>::
arclength(const unsigned int p) : p(p) {
}


template<int dim>
void
arclength<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}

template<int dim>
void
arclength<dim>::
run(const unsigned int global_refinement) {
	init(global_refinement);
	solve();
}


template<int dim>
void
arclength<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q-GLL finite element and boundary mapping
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	dealii::QGaussLobatto<1> support_points(p+1);
	grid->fe = std::make_shared< dealii::FE_Q<dim> > (support_points);
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	grid->distribute();
}


template<int dim>
void
arclength<dim>::
solve() {
Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	double approximated_arclenth{0.};
	
	dealii::QGaussLobatto<dim-1> quad_face ( grid->fe->tensor_degree()+1 );
	dealii::FEFaceValues<dim> fe_face_values(
		*(grid->mapping),
		*(grid->fe),
		quad_face,
// 		dealii::update_values |
// 		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell)
	if (cell->at_boundary()) {
		for (unsigned int face_no(0);
			face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
			// store iterator on the current face
			auto face = cell->face(face_no);
			
			if (face->at_boundary()) {
				// reinit fe_face_values since we have to integrate on this face
				fe_face_values.reinit(cell,face_no);
				
				for (unsigned int q(0); q < quad_face.size(); ++q) {
					approximated_arclenth +=
						1. * fe_face_values.JxW(q);
				}
			}
		} // loop over all faces of a cell
	} // loop over all cells (at boundary)
	
	////////////////////////////////////////////////////////////////////////////
	std::cout << "approximated arclength =: l = " << approximated_arclenth << std::endl;
	std::cout
		<< "error = | l - 2pi | = "
		<< std::abs( approximated_arclenth -2.*M_PI ) << std::endl;
}

} // namespace

#include "arclength.inst.in"
