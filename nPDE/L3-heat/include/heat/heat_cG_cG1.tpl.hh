/**
 * @file heat_cG_cG1.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-10-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief heat equation
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __heat_cG_cG1_tpl_hh
#define __heat_cG_cG1_tpl_hh

// PROJECT includes
#include <heat/Grid/Grid.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>

#include <deal.II/fe/fe.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace heat {

/** heat_cG_cG1 class.
 * @brief Handle heat PDE problem with p-FEM (cG(p)) using GLL-elements.
 */
template<int dim>
class heat_cG_cG1 {
public:
	heat_cG_cG1(const unsigned int p);
	virtual ~heat_cG_cG1() = default;
	
	virtual void set_grid(std::shared_ptr< heat::Grid<dim,1> > grid);
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	virtual void set_g(std::shared_ptr< dealii::Function<dim> > g);
	virtual void set_h(std::shared_ptr< dealii::Function<dim> > h);
	virtual void set_c(std::shared_ptr< dealii::Function<dim> > c);
	virtual void set_u0(std::shared_ptr< dealii::Function<dim> > u0);
	
	virtual void run(
		const unsigned int global_refinement,
		const double _t0, const double _T, const double _tau_n
	);

protected:
	virtual void init(const unsigned int global_refinement);
	
	virtual void assemble_system();
	virtual void assemble_f();
	virtual void assemble_h();
	
	virtual void solve(const double _t0, const double _T, const double _tau_n);
	
	virtual void write_vtk(dealii::Vector<double> &solution_vector);
	
	dealii::SparseMatrix<double> M;
	dealii::SparseMatrix<double> A;
	dealii::SparseMatrix<double> K;
	
	dealii::Vector<double> u0,u1; ///< Solution vector.
	
	dealii::Vector<double> b,bb; ///< Right hand side vector and tmp vector
	
	dealii::Vector<double> f,f0,f1; ///< Forcing term assembly vector.
	dealii::Vector<double> h,h0,h1; ///< Neumann boundary assembly vector.
	
	std::shared_ptr< heat::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > g; ///< Dirichlet boundary function.
		std::shared_ptr< dealii::Function<dim> > h; ///< Neumann boundary function.
		
		std::shared_ptr< dealii::Function<dim> > c; ///< Diffusion coefficient function.
		
		std::shared_ptr< dealii::Function<dim> > u0; ///< Initial values function.
	} function;
	
	const unsigned int p; ///< polynomial degree p
	
	int vtk_file_counter = {0};
};

} // namespace

#endif
