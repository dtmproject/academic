/**
 * @file Dirichlet0.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-11-30, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <condiff/BoundaryValues/Dirichlet0.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace condiff {

template <int dim>
double Dirichlet0<dim>::value (
	const dealii::Point<dim> &x,
	const unsigned int /* component */
) const {
	const double t = {this->get_time()};
	double val = {0.};
	
	if ( std::abs( x[0]-0. ) < 1.e-14 ) {
		val = 1.;
	}
	else {
		val = 0.;
	}
	
	return val * std::min(std::abs(t), 1.);
}

} // namespace

#include "Dirichlet0.inst.in"
