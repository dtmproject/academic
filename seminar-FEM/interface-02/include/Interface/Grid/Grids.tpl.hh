/**
 * @file Grids.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-11-11, UK
 * @date 2015-05-15, DTM++/AcousticWave Module, UK
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Grids_tpl_hh
#define __Grids_tpl_hh

// Project includes
#include <Interface/Grid/Grid.tpl.hh>

namespace Interface {

/** Grid4 (-1,1)^d
 * * Dirichlet boundary color on \f$ \partial \Omega \f$ .
 */
template<int dim, int spacedim>
class Grid4 : public Grid<dim,spacedim> {
public:
	Grid4() = default;
	virtual ~Grid4() = default;
	
	virtual void generate();
	virtual void set_boundary_indicators();
};

/** GridHyperBall
 * * Dirichlet boundary color on \f$ \partial \Omega \f$ .
 */
template<int dim, int spacedim>
class GridHyperBall : public Grid<dim,spacedim> {
public:
	GridHyperBall() = default;
	virtual ~GridHyperBall() = default;
	
	virtual void generate();
	virtual void set_boundary_indicators();
};


} // namespace

#endif
