/**
 * @file arclength.tpl.hh
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-30, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __arclength_tpl_hh
#define __arclength_tpl_hh

// PROJECT includes
#include <Arclength/Grid/Grid.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>

#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace Arclength {

template<int dim>
class arclength {
public:
	arclength(const unsigned int p);
	virtual ~arclength() = default;
	
	virtual void set_grid(std::shared_ptr< Arclength::Grid<dim,1> > grid);
	virtual void run(const unsigned int global_refinement);

protected:
	virtual void init(const unsigned int global_refinement);
	
	virtual void solve();
	
	std::shared_ptr< Arclength::Grid<dim,1> > grid;
	
	const unsigned int p; ///< polynomial degree p
};

} // namespace

#endif
