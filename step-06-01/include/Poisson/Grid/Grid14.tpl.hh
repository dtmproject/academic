/**
 * @file Grid14.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-08-11, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Grid14_tpl_hh
#define __Grid14_tpl_hh

// Project includes
#include <Poisson/Grid/Grid_DWR.tpl.hh>

namespace Poisson {

/** Grid14 (0,1)^d
 * * Dirichlet boundary color on \f$ \partial \Omega \f$ .
 */
template<int dim, int spacedim>
class Grid14 : public Grid_DWR<dim,spacedim> {
public:
	Grid14() = default;
	virtual ~Grid14() = default;
	
	virtual void generate();
	virtual void set_boundary_indicators();
};

} // namespace

#endif
