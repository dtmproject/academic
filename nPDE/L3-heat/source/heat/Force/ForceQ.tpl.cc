/**
 * @file ForceQ.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-11-08, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <heat/Force/ForceQ.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace heat {

template <int dim>
double ForceQ<dim>::value (
	const dealii::Point<dim> &x,
	const unsigned int /* component */
) const {
	const double t(this->get_time());
	Assert(
		(t >= 0.),
		dealii::ExcMessage("time t within this force value function must be t >= 0")
	);
	
	dealii::Point<dim> M;
	for (unsigned d(0); d < dim; ++d) {
		M[d] = 1.5;
	}
	
	const double r=1.0;
	
	double norm1 = {0};
	for (unsigned d(0); d < dim; ++d) {
		norm1 += std::abs(x[d]-M[d]);
	}
	
	double val = {0};
	
	if (norm1 < r) {
		val = 40.;
	}
	else {
		val =  0.;
	}
	
	return (t < 1.) ? t*val : val;
}

} // namespace

#include "ForceQ.inst.in"
