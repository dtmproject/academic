/**
 * @file ConvectionDiffusion_cG.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-08-15, UK
 * @date 2015-11-24, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief ConvectionDiffusion Problem
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __ConvectionDiffusion_cG_tpl_hh
#define __ConvectionDiffusion_cG_tpl_hh

// PROJECT includes
#include <ConvectionDiffusion/Grid/Grid.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/fe/fe.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace ConvectionDiffusion {

template<int dim>
class ConvectionDiffusion_cG {
public:
	ConvectionDiffusion_cG(const unsigned int p);
	virtual ~ConvectionDiffusion_cG() = default;
	
	virtual void set_grid(std::shared_ptr< ConvectionDiffusion::Grid<dim,1> > grid);
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	virtual void set_g(std::shared_ptr< dealii::Function<dim> > g);
	virtual void set_epsilon(std::shared_ptr< dealii::Function<dim> > epsilon);
	virtual void set_convection_tensor(
		std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor
	);
	
	virtual void run(const unsigned int global_refinement);

protected:
	virtual void init(const unsigned int global_refinement);
	
	virtual void assemble_system();
	virtual void assemble_f();
	
	virtual void solve();
	
	virtual void write_vtk(std::string name, dealii::Vector<double> &);
	
	dealii::SparseMatrix<double> A;    ///< System matrix.
	dealii::Vector<double> u;          ///< Solution vector.
	dealii::Vector<double> b;          ///< Linear system right hand side.
	
	dealii::Vector<double> f;          ///< Forcing terms vector.
	
	std::shared_ptr< ConvectionDiffusion::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > g; ///< Dirichlet boundary function.
		std::shared_ptr< dealii::Function<dim> > epsilon; ///< Diffusion coefficient function.
		std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor; ///< Convection tensor function.
	} function;
	
	const unsigned int p; ///< polynomial degree p
};

} // namespace

#endif
