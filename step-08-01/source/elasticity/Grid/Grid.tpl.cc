/**
 * @file Grid.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-13, UK
 * @date 2016-08-11, Poisson/DWR, UK
 * @date 2016-02-10, condiffrea, UK
 * @date 2016-01-14, condiff, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-05-15, DTM++/AcousticWave Module, UK
 * @date (2012-07-26), 2013-08-15, ElasticWave, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */


// PROJECT includes
#include <elasticity/Grid/Grid.tpl.hh>

// DTM++ includes
#include <DTM++/base/LogStream.hh>

// DEAL.II includes
#include <deal.II/lac/sparsity_pattern.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

// class declaration
namespace vectorvalued {

/// Constructor.
template<int dim>
Grid<dim>::
Grid() :
	tria(
		std::make_shared< dealii::Triangulation<dim> >(
			typename dealii::Triangulation<dim>::MeshSmoothing(
				dealii::Triangulation<dim>::smoothing_on_refinement
			)
		)
	) {
	
	primal.dof = std::make_shared< dealii::DoFHandler<dim> > (*tria);
	primal.constraints = std::make_shared< dealii::AffineConstraints<double> > ();
	primal.sp = std::make_shared< dealii::SparsityPattern >();
	
// 	dual.dof = std::make_shared< dealii::DoFHandler<dim> > (*tria);
// 	dual.constraints = std::make_shared< dealii::AffineConstraints<double> > ();
// 	dual.sp = std::make_shared< dealii::SparsityPattern >();
}


/// Destructor. Clears DoFHandler.
template<int dim>
Grid<dim>::
~Grid() {
	if (primal.dof.use_count())
		primal.dof->clear();
	
// 	if (dual.dof.use_count())
// 		dual.dof->clear();
}


/// Generate grid. Throws Exception in base class.
template<int dim>
void
Grid<dim>::
generate() {
	AssertThrow(false,
		dealii::ExcMessage("Grid::generate of base was called,"
		"here is nothing implemented!"));
}


/// Global refinement.
template<int dim>
void
Grid<dim>::
refine_global(const unsigned int n) {
	tria->refine_global(n);
	
	DTM::pout << "grid: tria: n_global_active_cells = "
		<< tria->n_global_active_cells()
		<< std::endl;
}


/// Set boundary indicators
template<int dim>
void
Grid<dim>::
set_boundary_indicators() {
	AssertThrow(false, dealii::ExcNotImplemented());
}


/// Distribute.
template<int dim>
void
Grid<dim>::
distribute() {
	// Distribute the degrees of freedom (dofs)
	
	DTM::pout << "grid: tria: n_global_active_cells = "
		<< tria->n_global_active_cells()
		<< std::endl;
	
	////////////////////////////////////////////////////////////////////////////
	// distribute primal dofs, create constraints and sparsity pattern sp
	{
		Assert(primal.dof.use_count(), dealii::ExcNotInitialized());
		Assert(primal.fe.use_count(), dealii::ExcNotInitialized());
		primal.dof->distribute_dofs(*primal.fe);
		
		DTM::pout << "grid: dof: primal mesh: n_dofs = " << primal.dof->n_dofs() << std::endl;
		
		// setup constraints like boundary values or hanging nodes
		Assert(primal.constraints.use_count(), dealii::ExcNotInitialized());
		primal.constraints->clear();
		primal.constraints->reinit();
		
		dealii::DoFTools::make_hanging_node_constraints(*primal.dof, *primal.constraints);
		dealii::VectorTools::interpolate_boundary_values(
			*primal.dof,
			static_cast< dealii::types::boundary_id > (
				vectorvalued::types::elasticity::boundary_id::Dirichlet
			),
			dealii::Functions::ZeroFunction<dim>(dim),
			*primal.constraints
		);
		
		primal.constraints->close();
		
		// Now we create a sparsity pattern, which we will use to initialise
		// our system matrix (for the assembly step).
		// See deal.II step-2 tutorial for details.
		dealii::DynamicSparsityPattern dsp(primal.dof->n_dofs(), primal.dof->n_dofs());
		dealii::DoFTools::make_sparsity_pattern(
			*primal.dof,
			dsp,
			*primal.constraints,
			false
		);
		
		Assert(primal.sp.use_count(), dealii::ExcNotInitialized());
		primal.sp->copy_from(dsp);
	}
}

} // namespaces

#include "Grid.inst.in"
