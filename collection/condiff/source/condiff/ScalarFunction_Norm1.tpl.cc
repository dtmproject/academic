/**
 * @file ScalarFunction_Norm1.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-04-05, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <condiff/ScalarFunction_Norm1.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace condiff {

template <int dim>
ScalarFunction_Norm1<dim>::
ScalarFunction_Norm1(
	dealii::Tensor<1,dim,double> _M,
	double _r,
	double _inner_value,
	double _outer_value
) : M(_M), r(_r), inner_value(_inner_value), outer_value(_outer_value) {
}


template <int dim>
double ScalarFunction_Norm1<dim>::value (
	const dealii::Point<dim> &p,
	const unsigned int /* component */
) const {
// 	std::vector<double> x(dim);
// 	for (unsigned int d=0; d < dim; ++d)
// 		x[d]=std::abs(p[d]-M[d]);
	
	// compute ||p - M||_1
	double norm_val = {0};
	for (unsigned int d=0; d < dim; ++d)
		norm_val += std::abs(p[d]-M[d]);
	
	return (norm_val <= r) ? inner_value : outer_value;
}

} // namespace

#include "ScalarFunction_Norm1.inst.in"
