/**
 * @file boundary_id.hh
 * @author Uwe Koecher (UK)
 * @date 2015-11-11, UK
 * @date 2015-05-15, UK
 *
 * @brief boundary_id
 * 
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/aWave. (Acoustic Wave Equation Solver)         */
/*                                                                            */
/*  DTM++/aWave is free software: you can redistribute it and/or modify       */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/aWave is distributed in the hope that it will be useful,            */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/aWave.   If not, see <http://www.gnu.org/licenses/>.     */


#ifndef __boundary_id_hh
#define __boundary_id_hh

namespace Arclength {
namespace types {

enum class boundary_id : unsigned int {
	Dirichlet = 1,
	Neumann = 2
};

}}

#endif
