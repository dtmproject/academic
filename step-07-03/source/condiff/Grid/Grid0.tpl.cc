/**
 * @file Grid0.tpl.cc
 * @author Uwe Koecher
 * @date 2016-08-11, condiff, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// Project includes
#include <condiff/Grid/Grid0.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/fe/mapping_q.h>

// C++ includes
#include <vector>

// class declaration
namespace condiff {

////////////////////////////////////////////////////////////////////////////////
// Grid0

template<int dim, int spacedim>
void
Grid0<dim,spacedim>::
generate() {
	const double a(0.);
	const double b(1.);
	
	dealii::GridGenerator::hyper_cube(
		*Grid<dim,spacedim>::tria,
		a,b,false
	);
}


template<int dim, int spacedim>
void
Grid0<dim,spacedim>::
set_boundary_indicators() {
	Assert((Grid<dim,spacedim>::tria.use_count()), dealii::ExcNotInitialized());
	
	// set boundary indicators
	auto cell(Grid<dim,spacedim>::tria->begin_active());
	auto endc(Grid<dim,spacedim>::tria->end());
	
	for (; cell != endc; ++cell) {
	if (cell->at_boundary()) {
	for (unsigned int face(0); face < dealii::GeometryInfo<dim>::faces_per_cell; ++face) {
		if (cell->face(face)->at_boundary()) {
			cell->face(face)->set_boundary_id(
				static_cast<dealii::types::boundary_id> (
					condiff::types::boundary_id::Dirichlet)
			);
		}
	}}}
}

} // namespaces

#include "Grid0.inst.in"
