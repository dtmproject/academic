/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2015-11-11, UK
 *
 * @brief DTM++.Project/academic/step-03-xx: Solve the ConvectionDiffusion problem.
 * 
 * @mainpage
 * <h2>ConvectionDiffusion problem</h2>
 * <h3>Partial differential equation and boundary values</h3>
 * \f[
 *   -\nabla \cdot \varepsilon \boldsymbol I_d \nabla u
 *   + \boldsymbol c \cdot \nabla u = f\,,\quad \text{in } \Omega\,,
 * \f]
 * with
 * \f[
 *   u(\boldsymbol x) = 0\,,\quad \text{on } \Gamma_D\,.
 * \f]
 * 
 * <h3>Variational form</h3>
 * Find \f$ u \in H^1_0(\Omega) \f$ from
 * \f[
 *   \int_\Omega \nabla v \cdot \varepsilon \boldsymbol I_d \nabla u \,\mathrm d \boldsymbol x
 *   + \int_\Omega v (\boldsymbol c \cdot \nabla u) \,\mathrm d \boldsymbol x
 *   = \int_\Omega v\, f\,\mathrm d \boldsymbol x\,,\quad
 *   \forall v \in H^1_0(\Omega)\,.
 * \f]
 * 
 * <h3>Default test problem #1</h3>
 * We approximate the exact solution
 * \f[ u^{E,0}(\boldsymbol x) = x(x-1)\, y(y-1) \f]
 * on \f$ \Omega = (0,1)^d \f$, \f$ d=2 \f$ with
 * \f$ \Gamma_D = \partial \Omega \f$ .
 * The grid description is ConvectionDiffusion::Grid0 .
 * 
 * The simulation data reads as:
 * \f[ f = f^0(\boldsymbol x) = -2y(y-1) -2x(x-1)\,. \f]
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// DEFINES

////////////////////////////////////////////////////////////////////////////////
// MPI Usage: Limit the numbers of threads to 1, since MPI+X has a poor
// performance (for at least Trilinos/Epetra).
// Undefine the variable USE_MPI_WITHOUT_THREADS if you want to use MPI+X,
// this would use as many threads per process as deemed useful by TBB.
#define USE_MPI_WITHOUT_THREADS

// We will further restrict to use a single process only,
// so we can enable threading parallelism safely by
#undef USE_MPI_WITHOUT_THREADS

#ifdef USE_MPI_WITHOUT_THREADS
#define MPIX_THREADS 1
#else
#define MPIX_THREADS dealii::numbers::invalid_unsigned_int
#endif
////////////////////////////////////////////////////////////////////////////////


// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <ConvectionDiffusion/ConvectionDiffusion_cG.tpl.hh>
#include <ConvectionDiffusion/Grid/Grids.tpl.hh>
#include <ConvectionDiffusion/Force/Force0.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>


int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	// EVALUATE wall time now.
	auto wall_time_start = MPI_Wtime();
	
	// Prepare DTM++ process logging to file
	DTM::pout.open();
	
	// Get MPI Variables
	const unsigned int MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD));
	const unsigned int NumProc(dealii::Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD));
	
	try {
		////////////////////////////////////////////////////////////////////////
		// Prepare output logging for deal.II
		//
		
		// Attach deallog to process output
		dealii::deallog.attach(DTM::pout);
		dealii::deallog.depth_console(0);
		DTM::pout
			<< "****************************************"
			<< "****************************************"
			<< std::endl
		;
		
		DTM::pout
			<< "Hej, here is process " << MyPID+1 << " from " << NumProc
			<< std::endl
		;
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Restrict usage to a single process (NumProc == 1) only.
		//
		
		AssertThrow(NumProc == 1, dealii::ExcMessage("MPI mode disabled."));
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		const unsigned int p = 1;
		const unsigned int global_refine = 5;
		
		const unsigned int DIM=1; // NOTE: try to set 2, 3 here!
		
		auto grid = std::make_shared< ConvectionDiffusion::Grid0<DIM,1> > ();
// 		auto f = std::make_shared< ConvectionDiffusion::Force0<DIM> > ();
		auto f = std::make_shared< dealii::Functions::ZeroFunction<DIM> > (1);
		
		auto epsilon = std::make_shared< dealii::Functions::ConstantFunction<DIM> > (1.e-2);
		
		auto problem = std::make_shared< ConvectionDiffusion::ConvectionDiffusion_cG<DIM> > (p);
		
		problem->set_grid(grid);
		problem->set_f(f);
		problem->set_epsilon(epsilon);
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		problem->run(global_refine);
		
		DTM::pout << std::endl << "Goodbye." << std::endl;
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////////<
	}
	catch (std::exception &exc) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
				<< std::endl
			;
			
			std::cerr << exc.what() << std::endl;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
			<< std::endl
		;
		
		DTM::pout << exc.what() << std::endl;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}
	catch (...) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An UNKNOWN EXCEPTION occured!"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl << std::endl
				<< "Further information:" << std::endl
				<< "\tThe main() function catched an exception"
				<< std::endl
				<< "\twhich is not inherited from std::exception."
				<< std::endl
				<< "\tYou have probably called 'throw' somewhere,"
				<< std::endl
				<< "\tif you do not have done this, please contact the authors!"
				<< std::endl << std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An UNKNOWN EXCEPTION occured!"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl << std::endl
			<< "Further information:" << std::endl
			<< "\tThe main() function catched an exception"
			<< std::endl
			<< "\twhich is not inherited from std::exception."
			<< std::endl
			<< "\tYou have probably called 'throw' somewhere,"
			<< std::endl
			<< "\tif you do not have done this, please contact the authors!"
			<< std::endl << std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}

	// EVALUATE program run time in terms of the consumed wall time.
	auto wall_time_end = MPI_Wtime();
	DTM::pout
		<< std::endl
		<< "Elapsed wall time: " << wall_time_end-wall_time_start
		<< std::endl
	;
	
	// Close output file stream
	DTM::pout.close();
	
	return 0;
}
