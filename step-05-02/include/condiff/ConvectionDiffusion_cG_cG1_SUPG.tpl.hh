/**
 * @file ConvectionDiffusion_cG_cG1_SUPG.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief ConvectionDiffusion Problem
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __ConvectionDiffusion_cG_cG1_SUPG_tpl_hh
#define __ConvectionDiffusion_cG_cG1_SUPG_tpl_hh

// PROJECT includes
#include <condiff/Grid/Grid.tpl.hh>

// DTM++ includes
#include <DTM++/io/DataOutput.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace condiff {

/** ConvectionDiffusion_cG_cG1_SUPG class.
 * @brief Handle ConvectionDiffusion's PDE problem with p-FEM (cG(p)) using GLL-elements.
 */
template<int dim>
class ConvectionDiffusion_cG_cG1_SUPG {
public:
	ConvectionDiffusion_cG_cG1_SUPG() = default;
	virtual ~ConvectionDiffusion_cG_cG1_SUPG() = default;
	
	virtual void set_grid(std::shared_ptr< condiff::Grid<dim,1> > grid);
	
	virtual void set_epsilon(
		std::shared_ptr< dealii::Function<dim> > epsilon
	);
	virtual void set_convection_tensor(
		std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor
	);
	
	virtual void set_f(std::shared_ptr< dealii::Function<dim> > f);
	virtual void set_h(std::shared_ptr< dealii::Function<dim> > h);
	virtual void set_u0(std::shared_ptr< dealii::Function<dim> > u0);
	
	virtual void set_data(
		const double t0,
		const double T,
		const double tau_n,
		const unsigned int p,
		const unsigned int global_refinement
	);
	
	virtual void set_delta_K(const double &delta_K);
	
	virtual void set_data_output_trigger(double data_output_trigger);
	virtual void set_data_output_patches(unsigned int data_output_patches);
	
	virtual void run();

protected:
	virtual void init(const unsigned int global_refinement);
	
	virtual void assemble_system();
	virtual void assemble_f(const double &t);
	virtual void assemble_h(const double &t);
	
	virtual void solve();
	
	virtual void do_data_output(
		double &trigger_time_variable, ///< external output time
		const double t0,               ///< left endpoint of I_n
		const double tn,               ///< right endpoint of I_n
		const double trigger_increment
	);
	
	dealii::SparseMatrix<double> M,A;  ///< System matrix.
	dealii::Vector<double> u0,u1;      ///< Solution vector.
	
	dealii::Vector<double> f0,f1;      ///< Forcing terms vector.
	dealii::Vector<double> h0,h1;      ///< Neumann boundary terms vector.
	
	
	std::shared_ptr< condiff::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> >
			epsilon; ///< Diffusion coefficient function.
		
		std::shared_ptr< dealii::TensorFunction<1,dim,double> >
			convection_tensor; ///< Convection tensor function.
		
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		std::shared_ptr< dealii::Function<dim> > h; ///< Neumann boundary function.
		std::shared_ptr< dealii::Function<dim> > u0; ///< Initial value function
	} function;
	
	
	struct {
		unsigned int p; ///< polynomial degree p
		unsigned int global_refinement;
		double t0;
		double T;
	} data;
	
	double tau_n;
	double delta_K; ///< SUPG stabilisation parameter
	
	// Data Output
	std::shared_ptr< dealii::Vector<double> > u_trigger; ///< Solution vector at trigger time
	DTM::DataOutput<dim> data_output;
	double data_output_trigger;
	double data_output_time_value;
	unsigned int data_output_patches;
};

} // namespace

#endif
