/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2016-01-15, condiff/SUPG, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 *
 * @brief DTM++.Project/academic/step-05: Solve the convection-diffusion problem.
 * 
 * @mainpage
 * <h2>Convection-Diffusion problem with Streamline Upwind/Petrov-Galerkin stabilisation (SUPG)</h2>
 * <h3>Partial differential equation, initial values and boundary values</h3>
 * \f[
 *   \partial_t u -\nabla \cdot \epsilon \nabla u + \boldsymbol b \cdot \nabla u = f\,,\quad \text{in } \Omega \times I\,,
 * \f]
 * with
 * \f[
 *   u(\boldsymbol x,0) = u_0\,,\quad \text{on } \Gamma \times \{0\}\,,
 * \f]
 * \f[
 *   u(\boldsymbol x,t) = 0\,,\quad \text{on } \Gamma_D \times I\,,
 * \f]
 * \f[
 *   \nabla u(\boldsymbol x,t) \cdot \boldsymbol n = h\,,\quad \text{on } \Gamma_N \times I\,.
 * \f]
 * 
 * <h3>Default test problem #1</h3>
 * We approximate the exact solution
 * \f[ u^{E,0}(\boldsymbol x, t) = \sin(\pi t) \sin(\pi x_1) \f]
 * on \f$ \Omega = (0,1)^d \f$, \f$ d=2 \f$ with
 * \f$ \Gamma_N = \{ \boldsymbol x \in \partial \Omega | x_d = 0 \lor x_d = 1 \} \f$
 * and
 * \f$ \Gamma_D = \partial \Omega \setminus \Gamma_N \f$ .
 * 
 * The grid description is condiff::Grid2 .
 * 
 * The time interval \f$ I=(0,T) \f$ with \f$ T=1 \f$.
 * 
 * The simulation data reads as:
 * \f[ u_0 = 0\,, \f]
 * \f[ h = 0\,, \f]
 * \f[ f = \pi \cos(\pi t) \sin(\pi x_1) + \pi^2 \sin(\pi t) \sin(\pi x_1) \,. \f]
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// DEFINES

////////////////////////////////////////////////////////////////////////////////
// MPI Usage: Limit the numbers of threads to 1, since MPI+X has a poor
// performance (for at least Trilinos/Epetra).
// Undefine the variable USE_MPI_WITHOUT_THREADS if you want to use MPI+X,
// this would use as many threads per process as deemed useful by TBB.
#define USE_MPI_WITHOUT_THREADS

// We will further restrict to use a single process only,
// so we can enable threading parallelism safely by
#undef USE_MPI_WITHOUT_THREADS

#ifdef USE_MPI_WITHOUT_THREADS
#define MPIX_THREADS 1
#else
#define MPIX_THREADS dealii::numbers::invalid_unsigned_int
#endif
////////////////////////////////////////////////////////////////////////////////


// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <condiff/ConvectionDiffusion_cG_cG1_SUPG.tpl.hh>
#include <condiff/Grid/Grids.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/function_parser.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>


int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	// EVALUATE wall time now.
	auto wall_time_start = MPI_Wtime();
	
	// Prepare DTM++ process logging to file
	DTM::pout.open();
	
	// Get MPI Variables
	const unsigned int MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD));
	const unsigned int NumProc(dealii::Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD));
	
	try {
		////////////////////////////////////////////////////////////////////////
		// Prepare output logging for deal.II
		//
		
		// Attach deallog to process output
		dealii::deallog.attach(DTM::pout);
		dealii::deallog.depth_console(0);
		DTM::pout
			<< "****************************************"
			<< "****************************************"
			<< std::endl
		;
		
		DTM::pout
			<< "Hej, here is process " << MyPID+1 << " from " << NumProc
			<< std::endl
		;
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Restrict usage to a single process (NumProc == 1) only.
		//
		
		AssertThrow(NumProc == 1, dealii::ExcMessage("MPI mode disabled."));
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		const unsigned int DIM=2;
		
		const unsigned int p = 3;
		const unsigned int global_refine = 2;
		
		const double t0 = 0.;
		const double T  = 1.;
		const double tau_n = 1e-2;
		
		const double delta_K = 1e-1;
		
		const double epsilon = 1e-7;
		
		dealii::Tensor<1,DIM,double> convection_tensor;
			convection_tensor[0] = 0.;
			convection_tensor[1] = 1.;
		
		double data_output_trigger = tau_n;
		unsigned int data_output_patches = p+4;
		
		
		////////////////////////////////////////////////////////////////////////
		// grid
		//
		auto grid = std::make_shared< condiff::Grid0<DIM,1> > ();
		
		////////////////////////////////////////////////////////////////////////
		// functions
		//
		auto epsilon_function =
			std::make_shared< dealii::ConstantFunction<DIM> > (epsilon);
		
		auto convection_tensor_function =
			std::make_shared< dealii::ConstantTensorFunction<1,DIM,double> > (
				convection_tensor
			);
		
// 		auto f = std::make_shared< dealii::ZeroFunction<DIM> > (1);
		auto h = std::make_shared< dealii::ZeroFunction<DIM> > (1);
		auto u0 = std::make_shared< dealii::ZeroFunction<DIM> > (1);
		
		// FunctionParser
		std::map<std::string, double> constants;
			constants["pi"] = dealii::numbers::PI;
		std::string variables;
		switch (DIM) {
			case 1: variables = "x0,t"; break;
			case 2: variables = "x0,x1,t"; break;
			case 3: variables = "x0,x1,x2,t"; break;
		}
		std::string expression;
		
		auto f = std::make_shared< dealii::FunctionParser<DIM> >(1);
		expression = "pi*cos(pi*t)*sin(pi*x0) + pi*pi*sin(pi*t)*sin(pi*x0)";
		f->initialize(variables,expression,constants,true);
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		auto problem = std::make_shared< condiff::ConvectionDiffusion_cG_cG1_SUPG<DIM> > ();
		
		problem->set_grid(grid);
		
		problem->set_epsilon(epsilon_function);
		problem->set_convection_tensor(convection_tensor_function);
		
		problem->set_f(f);
		problem->set_h(h);
		problem->set_u0(u0);
		
		problem->set_data(t0,T,tau_n, p, global_refine);
		problem->set_delta_K(delta_K);
		
		problem->set_data_output_trigger(data_output_trigger);
		problem->set_data_output_patches(data_output_patches);
		
		problem->run();
		
		DTM::pout << std::endl << "Goodbye." << std::endl;
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////////<
	}
	catch (std::exception &exc) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
				<< std::endl
			;
			
			std::cerr << exc.what() << std::endl;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
			<< std::endl
		;
		
		DTM::pout << exc.what() << std::endl;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}
	catch (...) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An UNKNOWN EXCEPTION occured!"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl << std::endl
				<< "Further information:" << std::endl
				<< "\tThe main() function catched an exception"
				<< std::endl
				<< "\twhich is not inherited from std::exception."
				<< std::endl
				<< "\tYou have probably called 'throw' somewhere,"
				<< std::endl
				<< "\tif you do not have done this, please contact the authors!"
				<< std::endl << std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An UNKNOWN EXCEPTION occured!"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl << std::endl
			<< "Further information:" << std::endl
			<< "\tThe main() function catched an exception"
			<< std::endl
			<< "\twhich is not inherited from std::exception."
			<< std::endl
			<< "\tYou have probably called 'throw' somewhere,"
			<< std::endl
			<< "\tif you do not have done this, please contact the authors!"
			<< std::endl << std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}

	// EVALUATE program run time in terms of the consumed wall time.
	auto wall_time_end = MPI_Wtime();
	DTM::pout
		<< std::endl
		<< "Elapsed wall time: " << wall_time_end-wall_time_start
		<< std::endl
	;
	
	// Close output file stream
	DTM::pout.close();
	
	return 0;
}
