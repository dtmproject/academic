/**
 * @file Neumann4.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-11-11, UK
 * @date 2015-11-09, UK
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Neumann4_tpl_hh
#define __Neumann4_tpl_hh

// PROJECT includes
#include <DTM++/base/Function.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>

// C++ includes

namespace Poisson {

template<int dim>
class Neumann4 : public DTM::Function<dim,1> {
public:
	Neumann4() = default;
	virtual ~Neumann4() = default;
	
	virtual double value (
		const dealii::Point<dim> &x,
		const unsigned int component = 0
	) const;
};

} // namespace

#endif
