/**
 * @file interface.tpl.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-30, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Interface/interface.tpl.hh>
#include <Interface/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/data_out.h>
// #include <deal.II/numerics/data_component_interpretation.h>

// C++ includes
#include <fstream>
#include <vector>

#include <set>

namespace Interface {

template<int dim>
interface<dim>::
interface(const unsigned int p) : p(p) {
}


template<int dim>
void
interface<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}

template<int dim>
void
interface<dim>::
set_diffusion(std::shared_ptr< dealii::Function<dim> > diffusion) {
	function.diffusion = diffusion;
}

template<int dim>
void
interface<dim>::
run(const unsigned int global_refinement) {
	init(global_refinement);
	solve();
}


template<int dim>
void
interface<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q-GLL finite element and boundary mapping
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	
	dealii::QGauss<1> support_points(p+1);
	grid->fe = std::make_shared< dealii::FE_DGQArbitraryNodes<dim> > (support_points);
	
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (p+1);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	grid->distribute();
	
	solution_vector.reinit(grid->dof->n_dofs());
}


template<int dim>
void
interface<dim>::
solve() {
Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	
	// TODO: loop over all cells and all verticies of a cell to find "an interface"
	// use cell->vertex( x ) to evaluate function.diffusion for instance
	// "store" the cell iterator in a std::set< x > cell_with_interface
	
	std::set< typename dealii::DoFHandler<dim>::active_cell_iterator > cells_on_interface;
	
	{
		Assert(function.diffusion.use_count(), dealii::ExcNotInitialized());
		
		auto cell = grid->dof->begin_active();
		auto endc = grid->dof->end();
		
		for ( ; cell != endc; ++cell ) {
			double c_on_vertex0 = function.diffusion->value(
				cell->vertex(0)
			);
			
			for (unsigned int vertex_no=1 ;
				vertex_no < dealii::GeometryInfo<dim>::vertices_per_cell ; ++vertex_no ) {
				
				if ( c_on_vertex0 != function.diffusion->value(cell->vertex(vertex_no)) ) {
// 					std::cout << "interface found on cell " << cell->index() << std::endl;
					
					cells_on_interface.emplace(cell);
					
					break;
				}
			}
		}
	}
	
	// TODO: visualize the result
	Assert(solution_vector.size(), dealii::ExcNotInitialized());
	solution_vector = 0.;
	
	std::cout << "Anzahl Zellen auf dem Interface = " << cells_on_interface.size() << std::endl;
	{
		for (auto &cell : cells_on_interface ) {
			std::cout << "interface found on cell " << cell->index() << std::endl;
			
			std::vector< dealii::types::global_dof_index > local_dof_indices(1);
			cell->get_dof_indices(local_dof_indices);
			Assert(local_dof_indices.size(), dealii::ExcNotInitialized());
			
			solution_vector(
				// global dof number
				local_dof_indices.front() // <=> local_dof_indices[0]
			) = 1.;
		}
		
		write_vtk("interface", solution_vector);
	}
}

template<int dim>
void
interface<dim>::
write_vtk(std::string name, dealii::Vector<double> &solution_vector) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	
	std::ofstream output_file(
		(name+".vtk").c_str()
	);
	
	std::vector<std::string> data_field_names;
	data_field_names.emplace_back("solution");
	
	std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation> dci_field;
	dci_field.emplace_back(dealii::DataComponentInterpretation::component_is_scalar);
	
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*(grid->dof));
	
	data_out.add_data_vector(
		solution_vector,
		data_field_names,
		dealii::DataOut<dim>::type_dof_data,
		dci_field
	);
	
	data_out.build_patches(std::min(static_cast<unsigned int>(1), p));
	
	data_out.write_vtk(output_file);
}

} // namespace

#include "interface.inst.in"
