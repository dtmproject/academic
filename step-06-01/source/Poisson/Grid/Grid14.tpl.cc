/**
 * @file Grid14.tpl.cc
 * @author Uwe Koecher
 * @date 2016-08-11, Poisson, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// Project includes
#include <Poisson/Grid/Grid14.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/fe/mapping_q.h>

// C++ includes
#include <vector>

// class declaration
namespace Poisson {

////////////////////////////////////////////////////////////////////////////////
// Grid14

template<int dim, int spacedim>
void
Grid14<dim,spacedim>::
generate() {
	Assert((dim==2), dealii::ExcNotImplemented());
	
	// create vertices
	std::vector< dealii::Point<dim> > vertices;
	std::vector< std::vector<unsigned int> > cells_vertices_index;
	
	if (dim==2) {
		vertices.push_back(dealii::Point<dim> (-1.,   -1.));
		vertices.push_back(dealii::Point<dim> (-1./2, -1.));
		vertices.push_back(dealii::Point<dim> (0.,    -1.));
		vertices.push_back(dealii::Point<dim> (+1./2, -1.));
		vertices.push_back(dealii::Point<dim> (+1,    -1.));
		
		vertices.push_back(dealii::Point<dim> (-1.,   -1./2.));
		vertices.push_back(dealii::Point<dim> (-1./2, -1./2.));
		vertices.push_back(dealii::Point<dim> (0.,    -1./2.));
		vertices.push_back(dealii::Point<dim> (+1./2, -1./2.));
		vertices.push_back(dealii::Point<dim> (+1,    -1./2.));
		
		vertices.push_back(dealii::Point<dim> (-1.,   0.));
		vertices.push_back(dealii::Point<dim> (-1./2, 0.));
		vertices.push_back(dealii::Point<dim> (+1./2, 0.));
		vertices.push_back(dealii::Point<dim> (+1,    0.));
		
		vertices.push_back(dealii::Point<dim> (-1.,   1./2.));
		vertices.push_back(dealii::Point<dim> (-1./2, 1./2.));
		vertices.push_back(dealii::Point<dim> (0.,    1./2.));
		vertices.push_back(dealii::Point<dim> (+1./2, 1./2.));
		vertices.push_back(dealii::Point<dim> (+1,    1./2.));
		
		vertices.push_back(dealii::Point<dim> (-1.,   1.));
		vertices.push_back(dealii::Point<dim> (-1./2, 1.));
		vertices.push_back(dealii::Point<dim> (0.,    1.));
		vertices.push_back(dealii::Point<dim> (+1./2, 1.));
		vertices.push_back(dealii::Point<dim> (+1,    1.));
		
		cells_vertices_index.push_back({0, 1, 5, 6});
		cells_vertices_index.push_back({1, 2, 6, 7});
		cells_vertices_index.push_back({2, 3, 7, 8});
		cells_vertices_index.push_back({3, 4, 8, 9});
		cells_vertices_index.push_back({5, 6, 10, 11});
		cells_vertices_index.push_back({8, 9, 12, 13});
		cells_vertices_index.push_back({10, 11, 14, 15});
		cells_vertices_index.push_back({12, 13, 17, 18});
		cells_vertices_index.push_back({14, 15, 19, 20});
		cells_vertices_index.push_back({15, 16, 20, 21});
		cells_vertices_index.push_back({16, 17, 21, 22});
		cells_vertices_index.push_back({17, 18, 22, 23});
	}
	
	// create deal.II conforming cell description
	std::vector< dealii::CellData<dim> > cells;
	cells.resize( cells_vertices_index.size() );
	
	for (unsigned int i=0; i < cells.size(); ++i) {
	for (unsigned int j=0; j < dealii::GeometryInfo<dim>::vertices_per_cell; ++j) {
		cells[i].vertices[j] = cells_vertices_index[i][j];
	}}
	
	// create deal.II triangulation
	Grid_DWR<dim,spacedim>::tria->create_triangulation(
		vertices,
		cells,
		dealii::SubCellData()
	);
	
	Grid_DWR<dim,spacedim>::tria->refine_global(1);
}


template<int dim, int spacedim>
void
Grid14<dim,spacedim>::
set_boundary_indicators() {
	Assert((Grid_DWR<dim,spacedim>::tria.use_count()), dealii::ExcNotInitialized());
	
	// set boundary indicators
	auto cell(Grid_DWR<dim,spacedim>::tria->begin_active());
	auto endc(Grid_DWR<dim,spacedim>::tria->end());
	
	for (; cell != endc; ++cell) {
	if (cell->at_boundary()) {
	for (unsigned int face(0); face < dealii::GeometryInfo<dim>::faces_per_cell; ++face) {
		if (cell->face(face)->at_boundary()) {
			cell->face(face)->set_boundary_id(
				static_cast<dealii::types::boundary_id> (
					Poisson::types::boundary_id::Dirichlet)
			);
		}
	}}}
}

} // namespaces

#include "Grid14.inst.in"
