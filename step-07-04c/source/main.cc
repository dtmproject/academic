/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2017-01-31, condiff with linear reaction, varying penalty, dG, UK
 * @date 2016-08-31, condiff dG, UK
 *
 * @brief DTM++.Project/academic/step-07: Solve the condiff with interior penalty methods.
 */

/*  Copyright (C) 2012-2017 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// DEFINES

////////////////////////////////////////////////////////////////////////////////
// MPI Usage: Limit the numbers of threads to 1, since MPI+X has a poor
// performance (for at least Trilinos/Epetra).
// Undefine the variable USE_MPI_WITHOUT_THREADS if you want to use MPI+X,
// this would use as many threads per process as deemed useful by TBB.
#define USE_MPI_WITHOUT_THREADS

// We will further restrict to use a single process only,
// so we can enable threading parallelism safely by
#undef USE_MPI_WITHOUT_THREADS

#ifdef USE_MPI_WITHOUT_THREADS
#define MPIX_THREADS 28
#else
#define MPIX_THREADS dealii::numbers::invalid_unsigned_int
#endif
////////////////////////////////////////////////////////////////////////////////


// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <condiff/condiff_dG.tpl.hh>
#include <condiff/Grid/Grids.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/function_parser.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>

#include <deal.II/base/convergence_table.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>


int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	// EVALUATE wall time now.
	auto wall_time_start = MPI_Wtime();
	
	// Prepare DTM++ process logging to file
	DTM::pout.open();
	
	// Get MPI Variables
	const unsigned int MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD));
	const unsigned int NumProc(dealii::Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD));
	
	try {
		////////////////////////////////////////////////////////////////////////
		// Prepare output logging for deal.II
		//
		
		// Attach deallog to process output
		dealii::deallog.attach(DTM::pout);
		dealii::deallog.depth_console(0);
		DTM::pout
			<< "****************************************"
			<< "****************************************"
			<< std::endl
		;
		
		DTM::pout
			<< "Hej, here is process " << MyPID+1 << " from " << NumProc
			<< std::endl
		;
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Restrict usage to a single process (NumProc == 1) only.
		//
		
		AssertThrow(NumProc == 1, dealii::ExcMessage("MPI mode disabled."));
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		const unsigned int p =  1;
		const double       S = -1; // SIPG: 1; NIPG: -1; IIPG: 0
		const unsigned int global_refine = 0; // 10: 1024, 15: 32768, 16: 65536
		const unsigned int convergence_range = 11;
// 		const unsigned int convergence_range = 4;
		
		const double epsilon_val = 1.0e-0;
		
		const double sigma0 = 1.0e1;
		
		// h-convergence loop (initialises and starts the problem for h-refinement)
		auto convergence_table = std::make_shared<dealii::ConvergenceTable> ();
		for (unsigned int refine=global_refine;
			refine < global_refine+convergence_range; ++refine) {
			////////////////////////////////////////////////////////////////////////
			// grid
			//
			auto grid = std::make_shared< condiff::Grid0<1,1> > ();
			
			const double h = {
				std::pow(2, -static_cast<double>(refine))
			};
			
			std::cout << "polynomial degree p = " << p << std::endl;
			
			////////////////////////////////////////////////////////////////////////
			// functions
			//
			
			////////////////////////////////////////////////////////////////////////
			// FunctionParser
			std::map<std::string, double> constants;
// 				constants["pi"] = dealii::numbers::PI;
				constants["epsilon"] = epsilon_val;
			std::string variables;
			switch (1) {
				case 1: variables = "x0,t"; break;
				case 2: variables = "x0,x1,t"; break;
				case 3: variables = "x0,x1,x2,t"; break;
			}
			std::string expression;
			////////////////////////////////////////////////////////////////////////
			
			std::cout << "epsilon = " << epsilon_val << std::endl;
			auto epsilon = std::make_shared< dealii::ConstantFunction<1> > (epsilon_val);
			
			dealii::Tensor<1,1> convection_tensor;
			convection_tensor[0] = 1;
			
			auto convection = std::make_shared< dealii::ConstantTensorFunction<1,1> > (convection_tensor);
			
			auto reaction = std::make_shared< dealii::ConstantFunction<1> > (1.0);
			
// 			auto f = std::make_shared< dealii::ZeroFunction<1> > ();
// 			auto f = std::make_shared< dealii::ConstantFunction<1> > (-2.0);
			auto f = std::make_shared< dealii::FunctionParser<1> >(1);
			expression = "( epsilon*(2+(-6+(-4+4*x0)*x0)*x0) + (-3+2*x0)*x0 )*exp(-x0*x0)";
			
			f->initialize(variables,expression,constants,true);
			
// 			auto u_D = std::make_shared< dealii::ZeroFunction<1> > ();
// 			auto u_D = std::make_shared< dealii::ConstantFunction<1> > (-2.0);
			auto u_D = std::make_shared< dealii::FunctionParser<1> >(1);
			expression = "(1.-x0)";
			u_D->initialize(variables,expression,constants,true);
			
			// penalty function
			auto gamma = std::make_shared< dealii::ConstantFunction<1> > (
				sigma0/h
			);
			
			// set exact solution (for error calculations)
			auto u = std::make_shared< dealii::FunctionParser<1> >(1);
			expression = "(1.-x0)*exp(-x0*x0)";
			u->initialize(variables,expression,constants,true);
			
			//
			////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////
			// Begin application
			//
			
			DTM::pout << "IP-dG method = ";
			if (S == 1)
				DTM::pout << "SIPG";
			if (S == -1)
				DTM::pout << "NIPG";
			if (S == 0)
				DTM::pout << "IIPG";
			DTM::pout << std::endl;
			
			std::cout << "penalty value gamma = sigma0 / h = "
				<< sigma0 << " / " << h << " = " << sigma0 / h
				<< std::endl;
			DTM::pout << "penalty value gamma = sigma0 / h = "
				<< sigma0 << " / " << h << " = " << sigma0 / h
				<< std::endl;
			
			auto problem = std::make_shared< condiff::condiff_dG<1> > ();
			
			problem->set_grid(grid);
			
			problem->set_epsilon(epsilon);
			problem->set_convection(convection);
			problem->set_reaction(reaction);
			
			problem->set_f(f);
			problem->set_u_D(u_D);
			
			
			problem->set_gamma(gamma);
			problem->set_data(p, S, refine);
			
			problem->set_u(u); // set exact solution for L^2(Omega) error evaluations
			
			problem->run();
			
			convergence_table->add_value("cells", std::pow(2, static_cast<double>(refine)));
			convergence_table->add_value("h", h);
			convergence_table->add_value("L2", problem->L2_error);
			
			DTM::pout << std::endl << "Goodbye." << std::endl;
			
			////////////////////////////////////////////////////////////////////////
		}
		
		////////////////////////////////////////////////////////////////////////
		// prepare and output convergence results
		convergence_table->set_precision("cells", 0);
		
		convergence_table->set_precision("h", 2);
		convergence_table->set_scientific("h", true);
		convergence_table->evaluate_convergence_rates(
			"h", dealii::ConvergenceTable::reduction_rate_log2
		);
		
		convergence_table->set_precision("L2", 4);
		convergence_table->set_scientific("L2", true);
		convergence_table->evaluate_convergence_rates(
			"L2", dealii::ConvergenceTable::reduction_rate_log2
		);
		
		std::cout << std::endl << "****** Convergence result ******" << std::endl;
		convergence_table->write_text(std::cout);
		
		DTM::pout << std::endl << "****** Convergence result ******" << std::endl;
		convergence_table->write_text(DTM::pout);
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////
	}
	catch (std::exception &exc) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
				<< std::endl
			;
			
			std::cerr << exc.what() << std::endl;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
			<< std::endl
		;
		
		DTM::pout << exc.what() << std::endl;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}
	catch (...) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An UNKNOWN EXCEPTION occured!"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl << std::endl
				<< "Further information:" << std::endl
				<< "\tThe main() function catched an exception"
				<< std::endl
				<< "\twhich is not inherited from std::exception."
				<< std::endl
				<< "\tYou have probably called 'throw' somewhere,"
				<< std::endl
				<< "\tif you do not have done this, please contact the authors!"
				<< std::endl << std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An UNKNOWN EXCEPTION occured!"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl << std::endl
			<< "Further information:" << std::endl
			<< "\tThe main() function catched an exception"
			<< std::endl
			<< "\twhich is not inherited from std::exception."
			<< std::endl
			<< "\tYou have probably called 'throw' somewhere,"
			<< std::endl
			<< "\tif you do not have done this, please contact the authors!"
			<< std::endl << std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}

	// EVALUATE program run time in terms of the consumed wall time.
	auto wall_time_end = MPI_Wtime();
	DTM::pout
		<< std::endl
		<< "Elapsed wall time: " << wall_time_end-wall_time_start
		<< std::endl
	;
	
	// Close output file stream
	DTM::pout.close();
	
	return 0;
}
