/**
 * @file ConvectionDiffusionReaction_cG_cG1_SUPG.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-02-10, condiffrea/SUPG, UK
 * @date 2016-01-15, condiff/SUPG, UK
 * @date 2016-01-14, condiff, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief ConvectionDiffusionReaction Problem
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <condiffrea/ConvectionDiffusionReaction_cG_cG1_SUPG.tpl.hh>
#include <condiffrea/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/linear_operator.h>
#include <deal.II/lac/packaged_operation.h>

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_bicgstab.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/numerics/data_out.h>

// C++ includes
#include <fstream>
#include <vector>

namespace condiffrea {

template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_epsilon(std::shared_ptr< dealii::Function<dim> > epsilon) {
	function.epsilon = epsilon;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_convection_tensor(
	std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor
) {
	function.convection_tensor = convection_tensor;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_reaction(std::shared_ptr< dealii::Function<dim> > reaction) {
	function.reaction = reaction;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_h(std::shared_ptr< dealii::Function<dim> > h) {
	function.h = h;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_u0(std::shared_ptr< dealii::Function<dim> > u0) {
	function.u0 = u0;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_data(
	const double t0,
	const double T,
	const double _tau_n,
	const unsigned int p,
	const unsigned int global_refinement) {
	data.p = p;
	data.global_refinement = global_refinement;
	
	data.t0 = t0;
	data.T = T;
	tau_n = _tau_n;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_delta_K(const double &_delta_K) {
	delta_K = _delta_K;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_data_output_trigger(double _data_output_trigger) {
	data_output_trigger = _data_output_trigger;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
set_data_output_patches(unsigned int _data_output_patches) {
	data_output_patches = _data_output_patches;
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
run() {
	solve();
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q-GLL finite element and boundary mapping
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	dealii::QGaussLobatto<1> support_points(data.p+1);
	grid->fe = std::make_shared< dealii::FE_Q<dim> > (support_points);
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (data.p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matricies
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u0.reinit(grid->dof->n_dofs());
	u1.reinit(grid->dof->n_dofs());
	
	f0.reinit(grid->dof->n_dofs());
	f1.reinit(grid->dof->n_dofs());
	
	h0.reinit(grid->dof->n_dofs());
	h1.reinit(grid->dof->n_dofs());
	
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	M.reinit(*(grid->sp));
	A.reinit(*(grid->sp));
	
	////////////////////////////////////////////////////////////////////////////
	// INIT DATA OUTPUT
	//
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u_trigger = std::make_shared< dealii::Vector<double> > ();
	u_trigger->reinit(grid->dof->n_dofs());
	
	// h5/vtu/vtk-data output trigger
	set_data_output_trigger(data_output_trigger);
	// h5/vtu/vtk-data output patches
	set_data_output_patches(data_output_patches);
	
	DTM::pout << "ConvectionDiffusionReaction: data output: trigger = " << data_output_trigger << std::endl;
	DTM::pout << "ConvectionDiffusionReaction: data output: patches = " << data_output_patches << std::endl;
	
	std::vector<std::string> data_field_names;
	data_field_names.push_back("u");
	
	std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci_field;
	dci_field.push_back(dealii::DataComponentInterpretation::component_is_scalar);
	
	data_output.set_DoF_data(
		grid->dof
	);
	
	data_output.set_data_field_names(data_field_names);
	data_output.set_data_component_interpretation_field(dci_field);
	data_output.set_data_output_patches(data_output_patches);
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.epsilon.use_count(), dealii::ExcNotInitialized());
	Assert(function.convection_tensor.use_count(), dealii::ExcNotInitialized());
	Assert(function.reaction.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	// NOTE: see deal.II step-3 tutorial for details
	// Here, we want to assemble the system matrix of the weak Laplacian operator.
	// Due to FEM, this can be done by summing up local contributions over
	// all cells and trial/test function combinations.
	// The inner most integral will be solved through a numical approximation
	// by a Gaussian quadrature rule.
	
	// Initialise the system matrix with 0.
	M = 0;
	A = 0;
	
	// Setup a Gaussian quadrature formula
	// NOTE: We take p+1 quadrature points
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |    // update shape function values
		dealii::update_gradients | // update shape function gradient values
		dealii::update_hessians |  // update 2nd derivatives of shape functions
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the assembly on each mesh cell
	// efficiently.
	// Afterwards they will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_M(
		grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
	);
	
	dealii::FullMatrix<double> local_A(
		grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblys. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	// We initialise it with the first active cell of our triangulation.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix for the cell assembly with 0.
		local_M = 0;
		local_A = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			const auto epsilon =
				function.epsilon->value(fe_values.quadrature_point(q) , 0);
			
			auto convection_tensor =
				function.convection_tensor->value(fe_values.quadrature_point(q));
			
			const auto reaction =
				function.reaction->value(fe_values.quadrature_point(q) , 0);
			
			// mass
			local_M(i,j) +=
				fe_values.shape_value(i,q) * fe_values.shape_value(j,q) *
				fe_values.JxW(q);
			
			// diffusion
			local_A(i,j) +=
				fe_values.shape_grad(i,q) *
				epsilon * fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
			
			// convection
			local_A(i,j) +=
				fe_values.shape_value(i,q) *
				convection_tensor * fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
			
			// linear reaction
			local_A(i,j) +=
				fe_values.shape_value(i,q) *
				reaction * fe_values.shape_value(j,q) *
				fe_values.JxW(q);
			
			// SUPG stabilisation terms (mass)
			local_M(i,j) +=
				convection_tensor * fe_values.shape_grad(i,q) * // test function
				delta_K *
				fe_values.shape_value(j,q) *
				fe_values.JxW(q);
			
			// SUPG stabilisation terms (stiffness)
			local_A(i,j) +=
				convection_tensor * fe_values.shape_grad(i,q) * // test function
				delta_K *
				(-epsilon) * dealii::trace(fe_values.shape_hessian(j,q)) *
				fe_values.JxW(q);
			
			local_A(i,j) +=
				convection_tensor * fe_values.shape_grad(i,q) * // test function
				delta_K *
				convection_tensor * fe_values.shape_grad(j,q) *
				fe_values.JxW(q);
			
			local_A(i,j) +=
				convection_tensor * fe_values.shape_grad(i,q) * // test function
				delta_K *
				reaction * fe_values.shape_value(j,q) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		Assert(
			(local_dof_indices.size() == grid->fe->dofs_per_cell),
			dealii::ExcNotInitialized()
		);
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
		
		grid->constraints->distribute_local_to_global(
			local_M, local_dof_indices, local_dof_indices, M
		);
		
		grid->constraints->distribute_local_to_global(
			local_A, local_dof_indices, local_dof_indices, A
		);
	}
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
assemble_f(const double &t) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	f1 = 0;
	function.f->set_time(t);
	
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	
	// Setup a FE_Values object.
	// This is needed to get the needed information from the FiniteElement
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_gradients |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			auto convection_tensor =
				function.convection_tensor->value(fe_values.quadrature_point(q));
			
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
			
			// SUPG stabilisation terms
			local_f(i) +=
				convection_tensor * fe_values.shape_grad(i,q) * // test function
				delta_K *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_f, local_dof_indices, f1
		);
	}
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
assemble_h(const double &t) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.h.use_count(), dealii::ExcNotInitialized());
	h1 = 0;
	function.h->set_time(t);
	
	dealii::QGaussLobatto<dim-1> quad_face ( grid->fe->tensor_degree()+1 );
	
	dealii::FEFaceValues<dim> fe_face_values(
		*(grid->mapping),
		*(grid->fe),
		quad_face,
		dealii::update_values |
		dealii::update_gradients |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_h(
		grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the global dof indices.
	// NOTE: We are using a C++ standart template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies. For this we need an iterator (kind of a pointer), which allows
	// us, to iterate over each cell easily.
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell)
	if (cell->at_boundary()) {
		// init local assemblies vector
		local_h = 0;
		
		// loop over all faces
		for (unsigned int face_no(0);
			face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
			// store iterator on the current face
			auto face = cell->face(face_no);
			
			// integrate on the Neumann boundary part only
			if (face->at_boundary() &&
				(face->boundary_id() ==
				static_cast<dealii::types::boundary_id>(condiffrea::types::boundary_id::Neumann))
			) {
				// reinit fe_face_values since we have to integrate on this face
				fe_face_values.reinit(cell,face_no);
				
				// Now loop over all shape function combinations and quadrature points
				// to get the assembly.
				for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
				for (unsigned int q(0); q < quad_face.size(); ++q) {
					auto convection_tensor =
						function.convection_tensor->value(fe_face_values.quadrature_point(q));
					
					local_h(i) +=
						fe_face_values.shape_value(i,q) *
						function.h->value(fe_face_values.quadrature_point(q), 0) *
						fe_face_values.JxW(q);
					
					// SUPG stabilisation terms
					local_h(i) +=
						convection_tensor * fe_face_values.shape_grad(i,q) * // test function
						delta_K *
						function.h->value(fe_face_values.quadrature_point(q), 0) *
						fe_face_values.JxW(q);
				}
			} // Neumann face
		} // loop over all faces of a cell
		
		// Get the global indices into the vector local_dof_indices.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assemblies to the global matrix.
		grid->constraints->distribute_local_to_global(
			local_h, local_dof_indices, h1
		);
	} // loop over all cells (at boundary)
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
solve() {
	const double _t0 = data.t0; // initial time
	const double _T = data.T;   // final time
	
	////////////////////////////////////////////////////////////////////////////
	init(data.global_refinement);
	
	assemble_system();
	const auto lop_M = dealii::linear_operator(M);
	const auto lop_A = dealii::linear_operator(A);
	
	////////////////////////////////////////////////////////////////////////////
	// Initialise time marching loop
	//
	double t0(-1);
	double t1(_t0);
	
	data_output_time_value = _t0;
	
	////////////////////////////////////////////////////////////////////////////
	// interpolate initial values onto grid / into initial solutions
	u1 = 0;
	dealii::VectorTools::interpolate(
		*grid->mapping,
		*grid->dof,
		*function.u0,
		u1
	);
	data_output.write_mesh("mesh", t0);
	
	f1 = 0;
	assemble_f(t1);
	
	h1 = 0;
	assemble_h(t1);
	
	////////////////////////////////////////////////////////////////////////////
	// Create linear system solver
	//
	auto solver_control = std::make_shared< dealii::SolverControl > (
		1000*grid->dof->n_dofs(), 1e-10, false, true
	);
	auto linear_system_solver = std::make_shared< dealii::SolverBicgstab< > > (
		*solver_control
	);
	
	////////////////////////////////////////////////////////////////////////////
	// Time marching loop
	//
	for (unsigned int n = 1; (_t0 + n*tau_n) < _T+tau_n; ++n) {
		t0 = t1;
		t1 = _t0 + n*tau_n;
		
		// swap data vectors
		u0 = u1;
		f0 = f1;
		h0 = h1;
		
		// assemble f1, h1
		assemble_f(t1);
		assemble_h(t1);
		
		// solve
		linear_system_solver->solve(
			(lop_M + tau_n/2 * lop_A),                          // matrix object
			u1,                                                 // solution vector
			((tau_n/2)*(f0+f1+h0+h1) + (lop_M - tau_n/2 * lop_A)*u0), // rhs object
			dealii::PreconditionIdentity()                      // preconditioner object
		);
		
		// write out the solution
		do_data_output(
			data_output_time_value,
			t0, t1,
			data_output_trigger
		);
	}
	
	////////////////////////////////////////////////////////////////////////////
}


template<int dim>
void
ConvectionDiffusionReaction_cG_cG1_SUPG<dim>::
do_data_output(double &t, const double t0, const double tn, const double trigger) {
	if (trigger <= 0) return;
	
	// trial functions on \hat I = [0,1]
	double xi0, xi1;
	// time on \hat I = [0,1]
	double _t;
	
	// trial function nodes on \hat I = [0,1]
	const double _t0 = 0;
	const double _t1 = 1./2.;
	
	// write data at triggered time
	for ( ; t < tn+(trigger/2.); t += trigger) {
		_t = (t - t0)/tau_n;
		
		xi0 = (_t - _t1)/(_t0 - _t1);
		xi1 = (_t - _t0)/(_t1 - _t0);
		
		u_trigger->equ(xi0, u0);
		u_trigger->add(xi1, u1);
		
		data_output.write_data("solution", u_trigger, t);
	}
}

} // namespace

#include "ConvectionDiffusionReaction_cG_cG1_SUPG.inst.in"
