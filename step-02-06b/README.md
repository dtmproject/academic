# README #

This README documents whatever steps are necessary to get the application
  DTM++.Project/academic/step-02-06b
up and running.

### What is this repository for? ###

* Poisson problem -Lu=f in Omega, inhom. Dirichlet. and inh. Neumann on a hyper_ball mesh
* Analytic solution: u = x^2 + y^2

### How do I get set up? ###

* Dependencies
deal.II v9.2.0 at least, installed via candi, cf. https://github.com/koecher/candi

* Configuration
cmake .
make

* Run (single process)
./app

* Run (multiple processes via MPI)
mpirun -np <Number of Processes> ./app


### Who do I talk to? ###

* Principial Author: Dr.-Ing. Dipl.-Ing. Uwe Koecher (koecher@hsu-hamburg.de)

### License ###
Copyright (C) 2012-2021 by Uwe Koecher

This file is part of DTM++.

DTM++ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

DTM++ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
 
You should have received a copy of the GNU Lesser General Public License
along with DTM++. If not, see <http://www.gnu.org/licenses/>.
Please see the file
	./LICENSE
for details.
