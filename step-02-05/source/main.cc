/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-22, inh. Dirichlet, direct solver, UK
 * @date 2015-11-11, UK
 *
 * @brief DTM++.Project/academic/step-02: Solve the Poisson problem.
 * 
 * @mainpage
 * <h2>Poisson problem</h2>
 * <h3>Partial differential equation and boundary values</h3>
 * \f[
 *   -\nabla \cdot \nabla u = f\,,\quad \text{in } \Omega = (-1,1)^2\,,
 * \f]
 * with
 * \f[
 *   u(\boldsymbol x) = x^2 + y^2\,,\quad \text{on } \Gamma_D\,,
 * \f]
 * \f[
 *   \nabla u(\boldsymbol x) \cdot \boldsymbol n = h\,,\quad \text{on } \Gamma_N\,.
 * \f]
 * 
 * <h3>Variational form</h3>
 * Find \f$ u \in V = H^1_0(\Gamma_D; \Omega) \cup H^1(\Gamma_N; \Omega) \f$ from
 * \f[
 *   \int_\Omega \nabla v \cdot \nabla u \,\mathrm d \boldsymbol x
 *   = \int_\Omega v\, f\,\mathrm d \boldsymbol x
 *   + \int_{\Gamma_N} v\, h\,\mathrm d \boldsymbol o\,,\quad
 *   \forall v \in V\,.
 * \f]
 * 
 * <h3>Default test problem #4</h3>
 * We approximate the exact solution
 * \f[ u^{E,0}(\boldsymbol x) = x^2 + y^2 \f]
 * on \f$ \Omega = (-1,1)^d \f$, \f$ d=2 \f$ with
 * \f$ \Gamma_N = \Big\{ \boldsymbol x = (x,y)^T \in \partial \Omega \, \Big| \,
 *                x = 0  \Big\} \f$
 * and
 * \f$ \Gamma_D = \partial \Omega \setminus \Gamma_N \f$ .
 * The grid description is Poisson::Grid4 .
 * 
 * Task: modify the code for the given Neumann boundary condition.
 * 
 * The simulation data reads as:
 * \f[ f = f^4(\boldsymbol x) = -4\,. \f]
 * \f[ g = g^4(\boldsymbol x) = x^2 + y^2\,. \f]
 * \f[ h = h^4(\boldsymbol x) = \,?\quad
 *         \text{with}\quad
 *         \boldsymbol n = (-1, 0)^T\quad \text{on } \Gamma_N  \,.
 * \f]
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// DEFINES

////////////////////////////////////////////////////////////////////////////////
// MPI Usage: Limit the numbers of threads to 1, since MPI+X has a poor
// performance (for at least Trilinos/Epetra).
// Undefine the variable USE_MPI_WITHOUT_THREADS if you want to use MPI+X,
// this would use as many threads per process as deemed useful by TBB.
#define USE_MPI_WITHOUT_THREADS

// We will further restrict to use a single process only,
// so we can enable threading parallelism safely by
#undef USE_MPI_WITHOUT_THREADS

#ifdef USE_MPI_WITHOUT_THREADS
#define MPIX_THREADS 1
#else
#define MPIX_THREADS dealii::numbers::invalid_unsigned_int
#endif
////////////////////////////////////////////////////////////////////////////////


// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <Poisson/Poisson_cG.tpl.hh>
#include <Poisson/Grid/Grids.tpl.hh>

#include <Poisson/Force/Force4.tpl.hh>
#include <Poisson/Dirichlet/Dirichlet4.tpl.hh>
#include <Poisson/Neumann/Neumann5.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>


int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	// EVALUATE wall time now.
	auto wall_time_start = MPI_Wtime();
	
	// Prepare DTM++ process logging to file
	DTM::pout.open();
	
	// Get MPI Variables
	const unsigned int MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD));
	const unsigned int NumProc(dealii::Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD));
	
	try {
		////////////////////////////////////////////////////////////////////////
		// Prepare output logging for deal.II
		//
		
		// Attach deallog to process output
		dealii::deallog.attach(DTM::pout);
		dealii::deallog.depth_console(0);
		DTM::pout
			<< "****************************************"
			<< "****************************************"
			<< std::endl
		;
		
		DTM::pout
			<< "Hej, here is process " << MyPID+1 << " from " << NumProc
			<< std::endl
		;
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Restrict usage to a single process (NumProc == 1) only.
		//
		
		AssertThrow(NumProc == 1, dealii::ExcMessage("MPI mode disabled."));
		
		//
		////////////////////////////////////////////////////////////////////////
		
		
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		const unsigned int p = 1;
		const unsigned int global_refine = 5;
		
		const unsigned int DIM=2;
		
		// approximate u = x^2 + y^2 on [-1, 1]^2
		
		auto grid = std::make_shared< Poisson::Grid5<DIM,1> > ();
		auto f = std::make_shared< Poisson::Force4<DIM> > ();
		auto g = std::make_shared< Poisson::Dirichlet4<DIM> > ();
		auto h = std::make_shared< Poisson::Neumann5<DIM> > ();
		
		auto problem = std::make_shared< Poisson::Poisson_cG<DIM> > (p);
		
		problem->set_grid(grid);
		problem->set_f(f);
		problem->set_g(g);
		problem->set_h(h);
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		problem->run(global_refine);
		
		DTM::pout << std::endl << "Goodbye." << std::endl;
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////////<
	}
	catch (std::exception &exc) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
				<< std::endl
			;
			
			std::cerr << exc.what() << std::endl;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
			<< std::endl
		;
		
		DTM::pout << exc.what() << std::endl;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}
	catch (...) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		DTM::pout
			<< std::endl
			<< "Elapsed wall time: " << wall_time_end-wall_time_start
			<< std::endl
		;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr
				<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An UNKNOWN EXCEPTION occured!"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl << std::endl
				<< "Further information:" << std::endl
				<< "\tThe main() function catched an exception"
				<< std::endl
				<< "\twhich is not inherited from std::exception."
				<< std::endl
				<< "\tYou have probably called 'throw' somewhere,"
				<< std::endl
				<< "\tif you do not have done this, please contact the authors!"
				<< std::endl << std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl
			;
			
			std::cerr
				<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
			;
		}
		
		// LOG error message to individual process output file.
		DTM::pout
			<< std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
			<< "An UNKNOWN EXCEPTION occured!"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl << std::endl
			<< "Further information:" << std::endl
			<< "\tThe main() function catched an exception"
			<< std::endl
			<< "\twhich is not inherited from std::exception."
			<< std::endl
			<< "\tYou have probably called 'throw' somewhere,"
			<< std::endl
			<< "\tif you do not have done this, please contact the authors!"
			<< std::endl << std::endl
			<< "----------------------------------------"
			<< "----------------------------------------"
			<< std::endl
		;
		
		DTM::pout
			<< std::endl
			<< "APPLICATION TERMINATED unexpectedly due to an exception."
			<< std::endl << std::endl
			<< "****************************************"
			<< "****************************************"
			<< std::endl << std::endl
		;
		
		// Close output file stream
		DTM::pout.close();
		
		return 1;
	}

	// EVALUATE program run time in terms of the consumed wall time.
	auto wall_time_end = MPI_Wtime();
	DTM::pout
		<< std::endl
		<< "Elapsed wall time: " << wall_time_end-wall_time_start
		<< std::endl
	;
	
	// Close output file stream
	DTM::pout.close();
	
	return 0;
}
