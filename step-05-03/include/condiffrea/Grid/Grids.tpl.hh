/**
 * @file Grids.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-02-10, condiffrea, UK
 * @date 2015-11-14, condiff, UK
 * @date 2015-11-11, UK
 * @date 2015-05-15, DTM++/AcousticWave Module, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Grids_tpl_hh
#define __Grids_tpl_hh

// Project includes
#include <condiffrea/Grid/Grid.tpl.hh>

namespace condiffrea {

/** Grid0 (0,1)^d
 * * Dirichlet boundary color on \f$ \partial \Omega \f$ .
 */
template<int dim, int spacedim>
class Grid0 : public Grid<dim,spacedim> {
public:
	Grid0() = default;
	virtual ~Grid0() = default;
	
	virtual void generate();
	virtual void set_boundary_indicators();
};

} // namespace

#endif
