/**
 * @file heat_cG_cG1.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2016-11-08, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief heat equation
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// PROJECT includes
#include <DTM++/base/LogStream.hh>

#include <heat/heat_cG_cG1.tpl.hh>
#include <heat/Grid/boundary_id.hh> 

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>

#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/full_matrix.h>

#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>

// C++ includes
#include <fstream>
#include <vector>

namespace heat {

template<int dim>
heat_cG_cG1<dim>::
heat_cG_cG1(const unsigned int p) : p(p) {
}


template<int dim>
void
heat_cG_cG1<dim>::
set_grid(std::shared_ptr< Grid<dim,1> > _grid) {
	grid = _grid;
}


template<int dim>
void
heat_cG_cG1<dim>::
set_f(std::shared_ptr< dealii::Function<dim> > f) {
	function.f = f;
}


template<int dim>
void
heat_cG_cG1<dim>::
set_g(std::shared_ptr< dealii::Function<dim> > g) {
	function.g = g;
}


template<int dim>
void
heat_cG_cG1<dim>::
set_h(std::shared_ptr< dealii::Function<dim> > h) {
	function.h = h;
}


template<int dim>
void
heat_cG_cG1<dim>::
set_c(std::shared_ptr< dealii::Function<dim> > c) {
	function.c = c;
}


template<int dim>
void
heat_cG_cG1<dim>::
set_u0(std::shared_ptr< dealii::Function<dim> > u0) {
	function.u0 = u0;
}


template<int dim>
void
heat_cG_cG1<dim>::
run(
	const unsigned int global_refinement,
	const double _t0, const double _T, const double _tau_n) {
	init(global_refinement);
	
	// time marching loop is implemented here:
	solve(_t0, _T, _tau_n);
}


template<int dim>
void
heat_cG_cG1<dim>::
init(const unsigned int global_refinement) {
	//generate FE_Q-GLL finite element and boundary mapping
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	dealii::QGaussLobatto<1> support_points(p+1);
	grid->fe = std::make_shared< dealii::FE_Q<dim> > (support_points);
	grid->mapping = std::make_shared< dealii::MappingQ<dim> > (p);
	
	// create grid and distribute dofs
	grid->generate();
	grid->refine_global(global_refinement);
	grid->set_boundary_indicators();
	grid->distribute();
	
	// now create vectors and matrices
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	u0.reinit(grid->dof->n_dofs());
	u1.reinit(grid->dof->n_dofs());
	
	b.reinit(grid->dof->n_dofs());
	bb.reinit(grid->dof->n_dofs());
	
	f.reinit(grid->dof->n_dofs());
	f0.reinit(grid->dof->n_dofs());
	f1.reinit(grid->dof->n_dofs());
	
	h.reinit(grid->dof->n_dofs());
	h0.reinit(grid->dof->n_dofs());
	h1.reinit(grid->dof->n_dofs());
	
	Assert(grid->sp.use_count(), dealii::ExcNotInitialized());
	M.reinit(*(grid->sp)); // Mass matrix
	A.reinit(*(grid->sp)); // Stiffness matrix
	K.reinit(*(grid->sp)); // K = M + (tau_n / 2) A
}


template<int dim>
void
heat_cG_cG1<dim>::
assemble_system() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.c.use_count(), dealii::ExcNotInitialized());
	
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	// NOTE: see deal.II step-3 tutorial for details
	// Here, we want to assemble the system matrix of the weak Laplacian operator.
	// Due to FEM, this can be done by summing up local contributions over
	// all cells and trial/test function combinations.
	// The inner most integral will be solved through a numerical approximation
	// by a quadrature rule.
	
	// Initialise the sparse system matrix entries with 0.
	M = 0;
	A = 0;
	
	// Setup a quadrature rule
	// NOTE: We take p+1 quadrature points
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	
	// Setup a FEValues<dim> object.
	// 
	// This is needed to get the needed information from the "FiniteElement",
	// which barely implements the local basis functions on the reference element.
	// 
	// It takes other information like a boundary-mapping (e.g. curved boundaries)
	// and a quadrature rule for numerical integration.
	// 
	// IMPORTANT: here you also specify the "update information", that are the
	// numerical quantities to be derived on each mesh cell like
	//   the shape function values, (if specified by update_values),
	//   the shape function gradients, (if specified by update_gradients),
	//   the quadrature weight times jacobian determinant, (if specified by update_JxW_values),
	//   and even more (as needed).
	// To be efficient, you may only specify the update information needed
	// for the assembly loop below.
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_quadrature_points |
		dealii::update_values |
		dealii::update_gradients |
		dealii::update_JxW_values
	);
	
	// Setup a (small) full matrix, to store the local assembly on each mesh cell
	// efficiently.
	// Afterwards this will be distributed into the global (sparse) matrix A.
	dealii::FullMatrix<double> local_M(
		grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
	);
	
	dealii::FullMatrix<double> local_A(
		grid->fe->dofs_per_cell, grid->fe->dofs_per_cell
	);
	
	// Setup a small vector, to store the local to global dof indices mapping.
	// NOTE: We are using a C++ standard template vector, not a deal.II
	// "Vector".
	// The FiniteElement object "fe" will tell us, how much dofs has each cell.
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	// Now we do the real work, looping over all cells to compute the cell
	// assemblies.
	auto cell = grid->dof->begin_active(); // mesh cell iterator object
	auto endc = grid->dof->end();          // loop abort iterator object
	
	for ( ; cell != endc; ++cell) {
		// First we have to compute the values of the gradients and
		// the JxW values.
		// The reinit of the fe_values object on the current cell will do this.
		fe_values.reinit(cell);
		
		// Initialise the full matrix entries for the cell assembly with 0.
		local_M = 0;
		local_A = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int j(0); j < grid->fe->dofs_per_cell; ++j)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_M(i,j) += 
				fe_values.shape_value(i,q) * fe_values.shape_value(j,q)
				* fe_values.JxW(q);
			
			local_A(i,j) +=
				// TODO: NOTE: solution: implement c(x)
				fe_values.shape_grad(i,q) *
				function.c->value(fe_values.quadrature_point(q), 0) *
				fe_values.shape_grad(j,q)
				* fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_M, local_dof_indices, M
		);
		
		grid->constraints->distribute_local_to_global(
			local_A, local_dof_indices, A
		);
	}
}


template<int dim>
void
heat_cG_cG1<dim>::
assemble_f() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	f = 0;
	
	dealii::QGaussLobatto<dim> quad ( grid->fe->tensor_degree()+1 );
	
	dealii::FEValues<dim> fe_values(
		*(grid->mapping),
		*(grid->fe),
		quad,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_f(
		grid->fe->dofs_per_cell
	);
	
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell) {
		// The reinit of the fe_values object on the current cell.
		fe_values.reinit(cell);
		
		// Initialise the vector for the cell assemblies with 0.
		local_f = 0;
		
		// Now loop over all shape function combinations and quadrature points
		// to get the assembly.
		for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
		for (unsigned int q(0); q < quad.size(); ++q) {
			local_f(i) +=
				fe_values.shape_value(i,q) *
				function.f->value(fe_values.quadrature_point(q), 0) *
				fe_values.JxW(q);
		}
		
		// Store the global indices into the vector local_dof_indices.
		// The cell object will give us the information.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assembly to the global matrix.
		// We use the constraint object, to set all constraints with that step.
		grid->constraints->distribute_local_to_global(
			local_f, local_dof_indices, f
		);
	}
}


template<int dim>
void
heat_cG_cG1<dim>::
assemble_h() {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->mapping.use_count(), dealii::ExcNotInitialized());
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(function.h.use_count(), dealii::ExcNotInitialized());
	h = 0;
	
	dealii::QGaussLobatto<dim-1> quad_face ( grid->fe->tensor_degree()+1 );
	
	dealii::FEFaceValues<dim> fe_face_values(
		*(grid->mapping),
		*(grid->fe),
		quad_face,
		dealii::update_values |
		dealii::update_quadrature_points |
		dealii::update_JxW_values
	);
	
	dealii::Vector<double> local_h(
		grid->fe->dofs_per_cell
	);
	
	std::vector< dealii::types::global_dof_index > local_dof_indices(
		grid->fe->dofs_per_cell
	);
	
	auto cell = grid->dof->begin_active();
	auto endc = grid->dof->end();
	
	for ( ; cell != endc; ++cell)
	if (cell->at_boundary()) {
		// init local assemblies vector
		local_h = 0;
		
		// loop over all faces
		for (unsigned int face_no(0);
			face_no < dealii::GeometryInfo<dim>::faces_per_cell; ++face_no) {
			// store iterator on the current face
			auto face = cell->face(face_no);
			
			// integrate on the Neumann boundary part only
			if (face->at_boundary() &&
				(face->boundary_id() == static_cast<dealii::types::boundary_id>(
				heat::types::boundary_id::Neumann))
			) {
				// reinit fe_face_values since we have to integrate on this face
				fe_face_values.reinit(cell,face_no);
				
				// Now loop over all shape function combinations and quadrature points
				// to get the assembly.
				for (unsigned int i(0); i < grid->fe->dofs_per_cell; ++i)
				for (unsigned int q(0); q < quad_face.size(); ++q) {
					local_h(i) +=
						// TODO: NOTE: solution: implement this line
						fe_face_values.shape_value(i,q) *
						function.h->value(fe_face_values.quadrature_point(q), 0) *
						fe_face_values.JxW(q);
				}
			} // Neumann face
		} // loop over all faces of a cell
		
		// Get the global indices into the vector local_dof_indices.
		cell->get_dof_indices(local_dof_indices);
		
		// Copy the local assemblies to the global matrix.
		grid->constraints->distribute_local_to_global(
			local_h, local_dof_indices, h
		);
	} // loop over all cells (at boundary)
}


template<int dim>
void
heat_cG_cG1<dim>::
solve(const double _t0, const double _T, const double _tau_n) {
	Assert(function.c.use_count(), dealii::ExcNotInitialized());
	Assert(function.f.use_count(), dealii::ExcNotInitialized());
	Assert(function.g.use_count(), dealii::ExcNotInitialized());
	Assert(function.h.use_count(), dealii::ExcNotInitialized());
	Assert(function.u0.use_count(), dealii::ExcNotInitialized());
	
	const double tend = _T;
	const double tau_n = _tau_n;
	
	// prepare time integration
	double t0 = {-1}; (void) t0;
	double t1 = {_t0};
	
	// interpolate u0 into vecto u1 (will be wrapped into vector u0 within the loop)
	function.u0->set_time(t1);
	u1 = 0;
	dealii::VectorTools::interpolate(
		*grid->mapping,
		*grid->dof,
		*function.u0,
		u1
	);
	
	// write interpolation of u0 to file
	write_vtk(u1);
	
	// assemble M, A
	assemble_system();
	
	// initial assembly of forcing terms and Neumann boundary function
	function.f->set_time(t1);
	assemble_f();
	f1=f;
	
	function.h->set_time(t1);
	assemble_h();
	h1=h;
	
	// create linear system matrix K = M + (tau_n/2) A (not necessary at all)
	K.copy_from(M);
	K.add(tau_n/2., A);
	
	////////////////////////////////////////////////////////////////////////////
	// prepare: linear system solver
	dealii::SolverControl sc(1000, 1e-14, false, true);
	dealii::SolverCG< > lss(sc);
	
	// time marching loop
	for (unsigned int n = 0; (_t0 + n*tau_n) < tend; ++n) {
		t0 = _t0 + n*tau_n; (void) t0;
		t1 = _t0 + (n+1)*tau_n;
		
		// wrap vectors
		u0 = u1;
		f0 = f1;
		h0 = h1;
		
		// assemble right hand side vectors
		function.f->set_time(t1);
		assemble_f();
		f1 = f;
		
		function.h->set_time(t1);
		assemble_h();
		h1 = h;
		
		////////////////////////////////////////////////////////////////////////
		// create linear system right hand side vector b
		A.vmult(bb, u0);
		
		b = 0;
		M.vmult(b, u0);       // M u^(n-1)
		b.add(-tau_n/2., bb); // -(tau/2) A u^(n-1)
		
		b.add(tau_n/2., f0); // Forcing terms
		b.add(tau_n/2., f1);
		
		b.add(tau_n/2., h0); // Neumann boundary terms
		b.add(tau_n/2., h1);
		
		////////////////////////////////////////////////////////////////////////
		// apply Dirichlet boundary values
		function.g->set_time(t1);
		
		std::map<dealii::types::global_dof_index, double> boundary_values_map;
		dealii::VectorTools::interpolate_boundary_values(
			*grid->dof,
			static_cast< dealii::types::boundary_id > (
				heat::types::boundary_id::Dirichlet
			),
			*(function.g),
			boundary_values_map
		);
		
		dealii::MatrixTools::apply_boundary_values(
			boundary_values_map,
			K,
			u1,
			b
		);
		
		////////////////////////////////////////////////////////////////////////
		// solve the linear system K u = b.
		lss.solve(
			K,u1,b,
			dealii::PreconditionIdentity()
		);
		
		write_vtk(u1);
	}
}


template<int dim>
void
heat_cG_cG1<dim>::
write_vtk(dealii::Vector<double> &solution_vector) {
	Assert(grid.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	
	std::string name="u";
	std::ostringstream filename;
	filename << name << "_"
		<< std::setw(4) << std::setfill('0') << vtk_file_counter++
		<< ".vtk";
	
	std::ofstream output_file(filename.str().c_str());
	
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*(grid->dof));
	data_out.add_data_vector(solution_vector, name);
	data_out.build_patches(grid->fe->tensor_degree());
	
	data_out.write_vtk(output_file);
}


} // namespace

#include "heat_cG_cG1.inst.in"
