/**
 * @file interface.tpl.hh
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-30, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __interface_tpl_hh
#define __interface_tpl_hh

// PROJECT includes
#include <Interface/Grid/Grid.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>

#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace Interface {

template<int dim>
class interface {
public:
	interface(const unsigned int p);
	virtual ~interface() = default;
	
	virtual void set_grid(std::shared_ptr< Interface::Grid<dim,1> > grid);
	virtual void set_diffusion(std::shared_ptr< dealii::Function<dim> > diffusion);
	virtual void run(const unsigned int global_refinement);

protected:
	virtual void init(const unsigned int global_refinement);
	
	virtual void solve();
	
	virtual void write_vtk(std::string name, dealii::Vector<double> &);
	
	std::shared_ptr< Interface::Grid<dim,1> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > diffusion;
		
	} function;
	
	dealii::Vector<double> solution_vector;
	
	const unsigned int p; ///< polynomial degree p
};

} // namespace

#endif
