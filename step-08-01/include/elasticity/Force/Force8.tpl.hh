/**
 * @file Force8.tpl.hh
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-13, UK
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Force8_tpl_hh
#define __Force8_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/point.h>

// C++ includes

namespace vectorvalued {

template<int dim>
class Force8 : public dealii::Function<dim> {
public:
	Force8() : dealii::Function<dim> (dim) {};
	
	Force8(
		const double r1,
		dealii::Point<dim> &p1,
		const double r2,
		dealii::Point<dim> &p2,
		const double r3,
		dealii::Point<dim> &p3
	);
	
	virtual ~Force8() = default;
	
	virtual double value (
		const dealii::Point<dim> &x,
		const unsigned int component
	) const;
	
	virtual void vector_value (
		const dealii::Point<dim> &x,
		dealii::Vector<double> &y
	) const;
	
private:
	double r1;
	dealii::Point<dim> p1;
	
	double r2;
	dealii::Point<dim> p2;
	
	double r3;
	dealii::Point<dim> p3;
};

} // namespace

#endif
