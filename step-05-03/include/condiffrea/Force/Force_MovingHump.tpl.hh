/**
 * @file Force_MovingHump.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2016-02-10, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __Force_MovingHump_tpl_hh
#define __Force_MovingHump_tpl_hh

// PROJECT includes
#include <DTM++/base/Function.tpl.hh>
#include <DTM++/base/TensorFunction.tpl.hh>

// DEAL.II includes
#include <deal.II/base/point.h>

// C++ includes

namespace condiffrea {

template<int dim>
class Force_MovingHump : public DTM::Function<dim,1> {
public:
	Force_MovingHump(
		const double r0,
		const double x0,
		const double y0,
		std::shared_ptr< dealii::Function<dim> > epsilon,
		std::shared_ptr< dealii::TensorFunction<1,dim,double> > convection_tensor,
		std::shared_ptr< dealii::Function<dim> > reaction
	);
	
	virtual ~Force_MovingHump() = default;
	
	virtual double value (
		const dealii::Point<dim> &x,
		const unsigned int component = 0
	) const;
	
private:
	double r0;
	double x0;
	double y0;
	
	struct {
		std::shared_ptr< dealii::Function<dim> >
			epsilon; ///< Diffusion coefficient function.
		
		std::shared_ptr< dealii::TensorFunction<1,dim,double> >
			convection_tensor; ///< Convection tensor function.
		
		std::shared_ptr< dealii::Function<dim> >
			reaction; ///< Linear reaction function.
	} function;
};

} // namespace

#endif
