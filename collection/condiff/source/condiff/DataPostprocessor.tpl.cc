/**
 * @file   DataPostprocessor.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2016-04-06, academic/heat, UK
 * @date   2015-02-24, CCFD, UK
 */

/*  Copyright (C) 2012-2016 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */


// PROJECT includes
#include <condiff/DataPostprocessor.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// C++ includes

namespace condiff {

template<int dim>
void
DataPostprocessor<dim>::
evaluate_scalar_field(
	const dealii::DataPostprocessorInputs::Scalar<dim> &input_data,
	std::vector< dealii::Vector<double> > &postprocessed_quantities
) const {
	auto &uh = input_data.solution_values;
	auto &d_uh = input_data.solution_gradients;
	auto &evaluation_points = input_data.evaluation_points;
	
	Assert(
		uh.size() != 0,
		dealii::ExcEmptyObject()
	);
	const unsigned int n_q_points(uh.size());
	
// 	Assert(
// 		uh[0].size() == 1, // only one solution component expected
// 		dealii::ExcInternalError()
// 	);
	
	Assert(
		postprocessed_quantities.size() == n_q_points,
		dealii::ExcInternalError()
	);
	
	Assert(
		postprocessed_quantities[0].size() == internal_n_components,
		dealii::ExcInternalError()
	);
	
	for (unsigned int q(0); q < n_q_points; ++q) {
		// store primal
		if (output_quantities & OutputQuantities::primal) {
			postprocessed_quantities[q][internal_primal_component] = uh[q];
		}
		
		// store flux components
		if (output_quantities & OutputQuantities::flux) {
			for (unsigned int k(0); k < dim; ++k) {
				postprocessed_quantities[q][internal_first_flux_component+k] =
					- function.epsilon->value(evaluation_points[q] , 0) *
					d_uh[q][k];
			}
		}
	}
}


template<int dim>
std::vector< std::string >
DataPostprocessor<dim>::
get_names() const {
	return postprocessed_quantities_names;
}


template<int dim>
std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation >
DataPostprocessor<dim>::
get_data_component_interpretation() const {
	return dci;
}


template<int dim>
dealii::UpdateFlags
DataPostprocessor<dim>::
get_needed_update_flags() const {
	return uflags;
}

} // namespace

#include "DataPostprocessor.inst.in"
