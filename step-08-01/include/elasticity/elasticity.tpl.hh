/**
 * @file elasticity.tpl.hh
 * @author Uwe Koecher (UK)
 * 
 * @date 2021-04-13, elasticity, UK
 * @date 2016-08-11, Poisson / DWR, UK
 * @date 2016-02-10, linear reaction, UK
 * @date 2016-01-12, UK
 * @date 2015-11-11, UK
 * @date 2015-11-04, UK
 *
 * @brief Elasticity Problem
 */

/*  Copyright (C) 2012-2021 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

#ifndef __elasticity_tpl_hh
#define __elasticity_tpl_hh

// PROJECT includes
#include <elasticity/Grid/Grid.tpl.hh>

// DTM++ includes
#include <DTM++/io/DataOutput.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/fe/fe.h>

#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>

namespace vectorvalued {

template<int dim>
class elasticity {
public:
	elasticity() = default;
	virtual ~elasticity() = default;
	
	virtual void set_grid(std::shared_ptr< vectorvalued::Grid<dim> > grid);
	
	virtual void set_f(
		std::shared_ptr< dealii::Function<dim> > f,
		std::shared_ptr< dealii::Function<dim> > lambda,
		std::shared_ptr< dealii::Function<dim> > mu
	);
	
	virtual void set_data(
		const unsigned int p_primal,
		const unsigned int global_refinement
	);
	
	virtual void set_data_output_patches(
		unsigned int data_output_patches_primal
	);
	
	virtual void run(const unsigned int refinement_cycles);

protected:
	virtual void init(const unsigned int global_refinement);
	virtual void reinit();
	
	// primal problem
	virtual void primal_assemble_system();
	virtual void primal_assemble_f();
	virtual void primal_solve();
	
	virtual void do_data_output(const double cycle);
	
	virtual void refine_grid();
	
	struct {
		dealii::SparseMatrix<double> A; ///< primal problem system matrix
		std::shared_ptr< dealii::Vector<double> > u; ///< primal problem solution
		dealii::Vector<double> f;       ///< primal problem rhs
		
		// Data Output
		DTM::DataOutput<dim> data_output;
		unsigned int data_output_patches;
	} primal;
	
	std::shared_ptr< vectorvalued::Grid<dim> > grid;
	
	struct {
		std::shared_ptr< dealii::Function<dim> > f; ///< Force function.
		
		std::shared_ptr< dealii::Function<dim> > lambda; ///< 1st Lame parameter function
		std::shared_ptr< dealii::Function<dim> > mu; ///< 2nd Lame parameter function
	} function;
	
	struct {
		unsigned int p_primal; ///< polynomial degree p of primal problem
		unsigned int global_refinement;
	} data;
};

} // namespace

#endif
